use nom::{character::complete::digit1, IResult, Parser};

pub fn number(stream: &str) -> IResult<&str, usize> {
    digit1.map(|x: &str| x.parse().unwrap()).parse(stream)
}
