use std::fmt::Debug;

use std::ops::{Deref, Index, IndexMut};

use itertools::Itertools;

#[derive(Copy, Clone)]
pub struct Grid<T, const X: usize, const Y: usize> {
    store: [[T; X]; Y],
}

impl<T, const X: usize, const Y: usize> Grid<T, X, Y> {
    fn is_index_valid(x: usize, y: usize) -> bool {
        x < X && y < Y
    }

    pub fn shift_index_by(x: usize, y: usize, dx: isize, dy: isize) -> Option<(usize, usize)> {
        let new_x = (x as isize + dx) as usize;
        let new_y = (y as isize + dy) as usize;

        if Self::is_index_valid(new_x, new_y) {
            Some((new_x, new_y))
        } else {
            None
        }
    }

    fn validated_offset_indices(
        x: usize,
        y: usize,
        offsets: &[(isize, isize)],
    ) -> Vec<(usize, usize)> {
        offsets
            .iter()
            .flat_map(|&(dx, dy)| Self::shift_index_by(x, y, dx, dy))
            .collect()
    }

    pub fn all_indices(&self) -> impl Iterator<Item = (usize, usize)> {
        (0..X).cartesian_product(0..Y)
    }

    pub fn neighbor_indices_cardinal(&self, x: usize, y: usize) -> Vec<(usize, usize)> {
        Self::validated_offset_indices(x, y, &[(-1, 0), (1, 0), (0, -1), (0, 1)])
    }

    pub fn neighbor_indices_corners(&self, x: usize, y: usize) -> Vec<(usize, usize)> {
        Self::validated_offset_indices(
            x,
            y,
            &[
                (-1, 0),
                (1, 0),
                (0, -1),
                (0, 1),
                (-1, -1),
                (-1, 1),
                (1, -1),
                (1, 1),
            ],
        )
    }

    pub fn from_digit_grid<const XX: usize, const YY: usize>(input: &str) -> Grid<u32, XX, YY> {
        let mut grid = Grid::default();

        for (y, line) in input.lines().enumerate() {
            for (x, ch) in line.chars().enumerate() {
                grid[(x, y)] = ch.to_digit(10).expect("Not a valid digit")
            }
        }

        grid
    }

    pub fn from_grid<const XX: usize, const YY: usize>(input: &str) -> Grid<char, XX, YY> {
        let mut grid = Grid::default();

        for (y, line) in input.lines().enumerate() {
            for (x, ch) in line.chars().enumerate() {
                grid[(x, y)] = ch
            }
        }

        grid
    }

    pub fn map<U>(&self, f: impl Fn(usize, usize, &T) -> U) -> Grid<U, X, Y>
    where
        U: Default + Copy,
    {
        let mut grid = Grid::default();

        for (x, y) in self.all_indices() {
            grid[(x, y)] = f(x, y, &self[(x, y)]);
        }

        grid
    }
}

impl<T, const X: usize, const Y: usize> Default for Grid<T, X, Y>
where
    T: Default + Copy,
{
    fn default() -> Self {
        Self {
            store: [[Default::default(); X]; Y],
        }
    }
}

impl<T, const X: usize, const Y: usize> Grid<T, X, Y>
where
    T: Default + Copy,
{
    pub fn from_rows(rows: &[impl Deref<Target = [T]>]) -> Self {
        let mut grid = Self::default();

        for (y, row) in rows.iter().enumerate() {
            for (x, item) in row.deref().iter().enumerate() {
                grid[(x, y)] = *item;
            }
        }

        grid
    }
}

impl<T, const X: usize, const Y: usize> Index<(usize, usize)> for Grid<T, X, Y> {
    type Output = T;

    fn index(&self, index: (usize, usize)) -> &Self::Output {
        &self.store[index.1][index.0]
    }
}

impl<T, const X: usize, const Y: usize> IndexMut<(usize, usize)> for Grid<T, X, Y> {
    fn index_mut(&mut self, index: (usize, usize)) -> &mut Self::Output {
        &mut self.store[index.1][index.0]
    }
}

impl<T, const X: usize, const Y: usize> Debug for Grid<T, X, Y>
where
    T: Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for row in &self.store {
            for item in row {
                write!(f, "{:?}", item)?;
            }
            writeln!(f)?;
        }

        Ok(())
    }
}
