use std::{
    collections::HashMap,
    ffi::OsString,
    path::{Path, PathBuf},
};

#[derive(Debug)]
pub enum FSNode {
    File(usize),
    Directory(HashMap<OsString, FSNode>),
}

impl FSNode {
    fn file(size: usize) -> Self {
        Self::File(size)
    }

    fn dir() -> Self {
        Self::Directory(HashMap::new())
    }

    fn find(&mut self, path: &Path) -> &mut Self {
        let mut here = self;
        for item in path.strip_prefix("/").unwrap().components() {
            let Self::Directory(items) = here else {
                panic!()
            };
            here = items.get_mut(item.as_os_str()).unwrap();
        }
        here
    }

    fn size(&self) -> usize {
        let mut total = 0;
        self.walk(&mut |x| {
            if let Self::File(s) = x {
                total += s;
            }
        });
        total
    }

    fn walk(&self, cb: &mut dyn FnMut(&Self)) {
        cb(self);
        if let Self::Directory(items) = self {
            for item in items.values() {
                item.walk(cb);
            }
        }
    }
}

pub fn input_generator(input: &str) -> FSNode {
    let mut root = FSNode::Directory(HashMap::new());
    let mut here = PathBuf::from("/");

    for item in input.lines() {
        if item.starts_with("$ ls") {
            // do nothing
        } else if let Some(dir) = item.strip_prefix("$ cd ") {
            match dir {
                "/" => here = PathBuf::from("/"),
                ".." => {
                    here.pop();
                }
                s => here.push(s),
            }
        } else if let Some(name) = item.strip_prefix("dir ") {
            let FSNode::Directory(items) = root.find(&here) else {
                panic!()
            };
            items.insert(name.into(), FSNode::dir());
        } else {
            let (size, name) = item.split_once(' ').unwrap();
            let size = size.parse().unwrap();
            let FSNode::Directory(items) = root.find(&here) else {
                panic!()
            };
            items.insert(name.into(), FSNode::file(size));
        }
    }

    root
}

pub fn solve_part1(input: &FSNode) -> usize {
    let mut total = 0;
    input.walk(&mut |x| {
        if let FSNode::Directory(_) = x {
            let sz = x.size();
            if sz <= 100000 {
                total += sz;
            }
        }
    });

    total
}

pub fn solve_part2(input: &FSNode) -> usize {
    let total = 70000000;
    let free = total - input.size();
    let need_freed = 30000000 - free;

    let mut best = total;
    input.walk(&mut |x| {
        let sz = x.size();
        if sz >= need_freed {
            best = best.min(sz);
        }
    });

    best
}
