use std::ops::Range;

fn parse_range(r: &str) -> Range<usize> {
    let (left, right) = r.split_once('-').unwrap();
    let left = left.parse().unwrap();
    let right: usize = right.parse().unwrap();
    Range {
        start: left,
        end: right + 1,
    }
}

pub fn input_generator(input: &str) -> Vec<(Range<usize>, Range<usize>)> {
    input
        .lines()
        .map(|chunk| {
            let (this, that) = chunk.split_once(',').unwrap();
            (parse_range(this), parse_range(that))
        })
        .collect()
}

pub fn solve_part1(input: &[(Range<usize>, Range<usize>)]) -> usize {
    input
        .iter()
        .filter(|&(this, that)| {
            (this.start >= that.start && this.end <= that.end)
                || (that.start >= this.start && that.end <= this.end)
        })
        .count()
}

pub fn solve_part2(input: &[(Range<usize>, Range<usize>)]) -> usize {
    input
        .iter()
        .filter(|&(this, that)| this.start.max(that.start) < this.end.min(that.end))
        .count()
}
