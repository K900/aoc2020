use std::collections::HashSet;

use itertools::Itertools;

pub fn input_generator(input: &str) -> Vec<(HashSet<char>, HashSet<char>)> {
    input
        .lines()
        .map(|chunk| {
            let length = chunk.len();
            let first_half = chunk.chars().take(length / 2).collect();
            let second_half = chunk.chars().skip(length / 2).collect();
            (first_half, second_half)
        })
        .collect()
}

fn char_to_code(c: char) -> usize {
    if c.is_ascii_lowercase() {
        return 1 + c as usize - 'a' as usize;
    }
    if c.is_ascii_uppercase() {
        return 27 + c as usize - 'A' as usize;
    }
    unreachable!()
}

pub fn solve_part1(input: &[(HashSet<char>, HashSet<char>)]) -> usize {
    input
        .iter()
        .map(|(f, s)| f.intersection(s).next().unwrap())
        .cloned()
        .map(char_to_code)
        .sum()
}

pub fn solve_part2(input: &[(HashSet<char>, HashSet<char>)]) -> usize {
    input
        .iter()
        .cloned()
        .map(|(f, s)| f.union(&s).cloned().collect::<HashSet<_>>())
        .chunks(3)
        .into_iter()
        .map(|c| {
            c.reduce(|acc, next| acc.intersection(&next).cloned().collect())
                .unwrap()
                .into_iter()
                .next()
                .unwrap()
        })
        .map(char_to_code)
        .sum()
}
