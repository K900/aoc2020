use super::cpu::{Cpu, Instruction};

pub fn input_generator(input: &str) -> Vec<Instruction> {
    input.lines().map(|x| x.parse().unwrap()).collect()
}

pub fn solve_part1(input: &[Instruction]) -> isize {
    let mut cpu = Cpu::new_with_insns(input);

    let mut total = 0;

    for tick in 1..=220 {
        cpu.tick();
        if matches!(tick, 19 | 59 | 99 | 139 | 179 | 219) {
            total += cpu.state.x * (tick + 1);
        }
    }

    total
}

pub fn solve_part2(input: &[Instruction]) -> isize {
    let mut cpu = Cpu::new_with_insns(input);

    for _ in 1..=6 {
        for cx in 0..40 {
            if (cpu.state.x - cx).abs() <= 1 {
                print!("#");
            } else {
                print!(" ");
            }
            cpu.tick();
        }
        println!();
    }

    0
}
