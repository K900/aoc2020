use std::cmp::Reverse;

pub fn input_generator(input: &str) -> Vec<Vec<usize>> {
    input
        .split("\n\n")
        .map(|chunk| {
            chunk
                .split('\n')
                .map(|x| x.trim().parse().unwrap())
                .collect()
        })
        .collect()
}

pub fn solve_part1(input: &[Vec<usize>]) -> usize {
    input.iter().map(|x| x.iter().sum()).max().unwrap()
}

pub fn solve_part2(input: &[Vec<usize>]) -> usize {
    let mut sums: Vec<_> = input.iter().map(|x| x.iter().sum()).collect();
    sums.sort_by_key(|&k| Reverse(k));
    sums[..3].iter().sum()
}
