use std::collections::HashSet;

#[derive(Copy, Clone)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    fn to_coordinates(self) -> (i64, i64) {
        match self {
            Direction::Up => (0, 1),
            Direction::Down => (0, -1),
            Direction::Left => (-1, 0),
            Direction::Right => (1, 0),
        }
    }
}

pub struct Command {
    direction: Direction,
    steps: usize,
}

fn debug_knot_grid(knots: &[(i64, i64)]) {
    println!("======");
    let mx = knots.iter().map(|x| x.0).max().unwrap();
    let my = knots.iter().map(|x| x.1).max().unwrap();

    for y in (0..=my).rev() {
        for x in 0..=mx {
            let dot = knots
                .iter()
                .enumerate()
                .filter_map(|(idx, i)| if *i == (x, y) { Some(idx) } else { None })
                .min()
                .map(|x| x.to_string())
                .unwrap_or_else(|| ".".into());
            print!("{}", dot);
        }
        println!();
    }
}

pub fn input_generator(input: &str) -> Vec<Command> {
    input
        .lines()
        .map(|l| {
            let (d, s) = l.split_once(' ').unwrap();
            let d = match d {
                "U" => Direction::Up,
                "D" => Direction::Down,
                "L" => Direction::Left,
                "R" => Direction::Right,
                _ => unreachable!(),
            };
            let s = s.parse().unwrap();
            Command {
                direction: d,
                steps: s,
            }
        })
        .collect()
}

fn solve(input: &[Command], length: usize) -> usize {
    let mut knots = vec![(0, 0); length];

    let mut seen = HashSet::new();
    seen.insert((0, 0));

    for cmd in input {
        for _ in 0..cmd.steps {
            let (dhx, dhy) = cmd.direction.to_coordinates();

            knots[0].0 += dhx;
            knots[0].1 += dhy;

            for i in 0..knots.len() - 1 {
                let this = knots[i];
                let next = knots[i + 1];
                let (dtx, dty) = match ((this.0 - next.0), (this.1 - next.1)) {
                    // close enough
                    (-1, -1)
                    | (-1, 0)
                    | (-1, 1)
                    | (0, -1)
                    | (0, 0)
                    | (0, 1)
                    | (1, -1)
                    | (1, 0)
                    | (1, 1) => (0, 0),

                    // two away in a line
                    (-2, 0) => (-1, 0),
                    (2, 0) => (1, 0),
                    (0, -2) => (0, -1),
                    (0, 2) => (0, 1),

                    // L away
                    (-2, -1) => (-1, -1),
                    (-2, 1) => (-1, 1),
                    (2, -1) => (1, -1),
                    (2, 1) => (1, 1),
                    (-1, 2) => (-1, 1),
                    (-1, -2) => (-1, -1),
                    (1, 2) => (1, 1),
                    (1, -2) => (1, -1),

                    // long diagonal
                    (-2, -2) => (-1, -1),
                    (-2, 2) => (-1, 1),
                    (2, -2) => (1, -1),
                    (2, 2) => (1, 1),

                    _ => {
                        debug_knot_grid(&knots);
                        panic!(
                            "Knots too far apart at index {}, head at {:?}, tail at {:?}",
                            i, this, next
                        )
                    }
                };

                knots[i + 1].0 += dtx;
                knots[i + 1].1 += dty;
            }

            // debug_knot_grid(&knots);

            seen.insert(knots[length - 1]);
        }
    }

    seen.len()
}

pub fn solve_part1(input: &[Command]) -> usize {
    solve(input, 2)
}

pub fn solve_part2(input: &[Command]) -> usize {
    solve(input, 10)
}
