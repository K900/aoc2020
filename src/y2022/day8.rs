use crate::utils::grid::Grid;

type G = Grid<u32, 99, 99>;

pub fn input_generator(input: &str) -> G {
    G::from_digit_grid(input)
}

fn check_occluded_one(grid: &G, x: usize, y: usize, dx: isize, dy: isize) -> (bool, usize) {
    let (mut cx, mut cy) = (x, y);
    let mut count = 0;
    while let Some((nx, ny)) = G::shift_index_by(cx, cy, dx, dy) {
        count += 1;
        if grid[(nx, ny)] >= grid[(x, y)] {
            return (true, count);
        }
        (cx, cy) = (nx, ny);
    }

    (false, count)
}

pub fn solve_part1(input: &G) -> usize {
    input
        .all_indices()
        .filter(|&(x, y)| {
            [(-1, 0), (1, 0), (0, 1), (0, -1)]
                .into_iter()
                .any(|(dx, dy)| !check_occluded_one(input, x, y, dx, dy).0)
        })
        .count()
}

pub fn solve_part2(input: &G) -> usize {
    input
        .all_indices()
        .map(|(x, y)| {
            [(-1, 0), (1, 0), (0, 1), (0, -1)]
                .into_iter()
                .map(|(dx, dy)| check_occluded_one(input, x, y, dx, dy).1)
                .product()
        })
        .max()
        .unwrap()
}
