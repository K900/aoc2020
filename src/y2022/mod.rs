mod day1;
mod day10;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;

mod cpu;

aoc_main::main! {
    year 2022;
    day1 : input_generator => solve_part1, solve_part2;
    day2 : input_generator => solve_part1, solve_part2;
    day3 : input_generator => solve_part1, solve_part2;
    day4 : input_generator => solve_part1, solve_part2;
    day5 : input_generator => solve_part1, solve_part2;
    day6 => solve_part1, solve_part2;
    day7 : input_generator => solve_part1, solve_part2;
    day8 : input_generator => solve_part1, solve_part2;
    day9 : input_generator => solve_part1, solve_part2;
    day10 : input_generator => solve_part1, solve_part2;
}

pub fn run() {
    main()
}
