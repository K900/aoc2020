use std::str::FromStr;

#[derive(Debug, Copy, Clone)]
pub struct CpuState {
    pub x: isize,
}

#[derive(Debug)]
pub struct Cpu {
    pub state: CpuState,
    pc: usize,
    insns: Vec<Uop>,
}

pub enum Instruction {
    AddX(isize),
    Nop,
}

impl std::fmt::Debug for Instruction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::AddX(arg) => write!(f, "addx {}", arg),
            Self::Nop => write!(f, "nop"),
        }
    }
}

impl FromStr for Instruction {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(rest) = s.strip_prefix("addx ") {
            Ok(Self::AddX(rest.parse().unwrap()))
        } else if s == "noop" {
            Ok(Self::Nop)
        } else {
            unreachable!()
        }
    }
}

#[derive(Debug, Copy, Clone)]
enum Uop {
    AddX(isize),
    Nop,
}

impl CpuState {
    pub fn new() -> Self {
        Self { x: 1 }
    }
}

fn decode(insns: &[Instruction]) -> Vec<Uop> {
    let mut result = vec![];
    for i in insns {
        match i {
            Instruction::AddX(a) => {
                result.push(Uop::Nop);
                result.push(Uop::AddX(*a));
            }
            Instruction::Nop => result.push(Uop::Nop),
        };
    }

    result
}

impl Cpu {
    pub fn new_with_insns(insns: &[Instruction]) -> Self {
        Self {
            state: CpuState::new(),
            pc: 0,
            insns: decode(insns),
        }
    }

    pub fn tick(&mut self) {
        match self.insns[self.pc] {
            Uop::AddX(a) => {
                self.state.x += a;
            }
            Uop::Nop => {}
        }
        self.pc += 1;
    }
}
