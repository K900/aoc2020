use nom::{bytes::complete::tag, character::complete::digit1, sequence::tuple, IResult, Parser};

#[derive(Clone, Debug)]
pub struct CrateStacks {
    stacks: Vec<Vec<char>>,
}

impl CrateStacks {
    fn apply_1(&mut self, m: &Move) {
        for _ in 0..m.count {
            let item = self.stacks[m.from - 1].pop().unwrap();
            self.stacks[m.to - 1].push(item);
        }
    }

    fn apply_2(&mut self, m: &Move) {
        let src = &mut self.stacks[m.from - 1];
        let topn: Vec<_> = src.drain(src.len() - m.count..).collect();
        let dst = &mut self.stacks[m.to - 1];
        dst.extend_from_slice(&topn);
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Move {
    from: usize,
    to: usize,
    count: usize,
}

pub fn input_generator(input: &str) -> (CrateStacks, Vec<Move>) {
    let (crates, moves) = input.split_once("\n\n").unwrap();

    let clean_crates: Vec<Vec<_>> = crates
        .split('\n')
        .map(|r| r.chars().skip(1).step_by(4).collect())
        .collect();

    let rotated_crates: Vec<Vec<_>> = (0..clean_crates[0].len())
        .map(|i| {
            clean_crates
                .iter()
                .rev()
                .skip(1)
                .map(|r| r[i])
                .filter(|&c| c != ' ')
                .collect()
        })
        .collect();

    let moves = moves
        .lines()
        .map(|l| {
            let result: IResult<&str, Move> = tuple((
                tag("move "),
                digit1,
                tag(" from "),
                digit1,
                tag(" to "),
                digit1,
            ))
            .map(
                |(_, count, _, from, _, to): (_, &str, _, &str, _, &str)| Move {
                    from: from.parse().unwrap(),
                    to: to.parse().unwrap(),
                    count: count.parse().unwrap(),
                },
            )
            .parse(l);

            result.unwrap().1
        })
        .collect();

    (
        CrateStacks {
            stacks: rotated_crates,
        },
        moves,
    )
}

pub fn solve_part1((stacks, moves): &(CrateStacks, Vec<Move>)) -> String {
    let mut stacks = stacks.clone();
    for m in moves {
        stacks.apply_1(m)
    }

    stacks.stacks.iter().map(|s| s.last().unwrap()).collect()
}

pub fn solve_part2((stacks, moves): &(CrateStacks, Vec<Move>)) -> String {
    let mut stacks = stacks.clone();
    for m in moves {
        stacks.apply_2(m)
    }

    stacks.stacks.iter().map(|s| s.last().unwrap()).collect()
}
