#[derive(Copy, Clone, PartialEq, Eq)]
pub enum Move {
    Rock,
    Paper,
    Scissors,
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub enum Outcome {
    Win,
    Lose,
    Draw,
}

impl Move {
    fn beats(self) -> Self {
        match self {
            Move::Rock => Move::Scissors,
            Move::Paper => Move::Rock,
            Move::Scissors => Move::Paper,
        }
    }

    fn loses_to(self) -> Self {
        match self {
            Move::Rock => Move::Paper,
            Move::Paper => Move::Scissors,
            Move::Scissors => Move::Rock,
        }
    }

    fn outcome(self, other: Self) -> Outcome {
        if self.beats() == other {
            Outcome::Win
        } else if self.loses_to() == other {
            Outcome::Lose
        } else {
            Outcome::Draw
        }
    }

    fn score(self) -> usize {
        match self {
            Move::Rock => 1,
            Move::Paper => 2,
            Move::Scissors => 3,
        }
    }
}

impl Outcome {
    fn score(self) -> usize {
        match self {
            Outcome::Win => 6,
            Outcome::Lose => 0,
            Outcome::Draw => 3,
        }
    }
}

pub fn input_generator(input: &str) -> Vec<(Move, Move)> {
    input
        .lines()
        .map(|line| {
            let (they, you) = line.split_once(' ').unwrap();
            let they = match they {
                "A" => Move::Rock,
                "B" => Move::Paper,
                "C" => Move::Scissors,
                _ => unreachable!(),
            };
            let you = match you {
                "X" => Move::Rock,
                "Y" => Move::Paper,
                "Z" => Move::Scissors,
                _ => unreachable!(),
            };
            (you, they)
        })
        .collect()
}

pub fn solve_part1(input: &[(Move, Move)]) -> usize {
    input
        .iter()
        .map(|&(you, them)| you.score() + you.outcome(them).score())
        .sum()
}

pub fn solve_part2(input: &[(Move, Move)]) -> usize {
    input
        .iter()
        .map(|&(you, them)| {
            let expected_outcome = match you {
                Move::Rock => Outcome::Lose,
                Move::Paper => Outcome::Draw,
                Move::Scissors => Outcome::Win,
            };

            let expected_move = match expected_outcome {
                Outcome::Win => them.loses_to(),
                Outcome::Lose => them.beats(),
                Outcome::Draw => them,
            };

            expected_move.score() + expected_outcome.score()
        })
        .sum()
}
