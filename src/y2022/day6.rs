use itertools::Itertools;

fn solve(input: &str, size: usize) -> usize {
    input
        .as_bytes()
        .windows(size)
        .find_position(|w| w.iter().all_unique())
        .unwrap()
        .0
        + size
}

pub fn solve_part1(input: &str) -> usize {
    solve(input, 4)
}

pub fn solve_part2(input: &str) -> usize {
    solve(input, 14)
}
