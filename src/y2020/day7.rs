use std::str::FromStr;

use petgraph::{graphmap::DiGraphMap, visit::Dfs};

pub fn input_generator(input: &str) -> DiGraphMap<&str, usize> {
    let mut graph = DiGraphMap::new();

    for line in input.lines() {
        let (base, rest) = line
            .split_once(" bags contain ")
            .expect("line must contain 'bags contain'");

        for part in rest.split(", ") {
            if part == "no other bags." {
                continue;
            }

            let (n, rest) = part.split_once(' ').expect("part must contain a space");
            let n = usize::from_str(n).expect("part must be a valid number");
            let (kind, _) = rest.rsplit_once(' ').expect("rest must contain space");

            graph.add_edge(kind, base, n);
        }
    }

    graph
}

pub fn solve_part1(graph: &DiGraphMap<&str, usize>) -> usize {
    let mut count = 0;

    let mut dfs = Dfs::new(graph, "shiny gold");
    while dfs.next(graph).is_some() {
        count += 1;
    }

    count - 1 // we count the start node, they don't
}

pub fn solve_part2(graph: &DiGraphMap<&str, usize>) -> usize {
    let mut queue = vec![("shiny gold", 1)];
    let mut total = 0;

    while let Some((color, multiplier)) = queue.pop() {
        for neighbor in graph.neighbors_directed(color, petgraph::EdgeDirection::Incoming) {
            let this_weight = *graph
                .edge_weight(neighbor, color)
                .expect("graph must contain weight");
            queue.push((neighbor, multiplier * this_weight))
        }
        total += multiplier
    }

    total - 1 // again, don't count the start node
}
