use std::str::FromStr;

pub fn input_generator(input: &str) -> Vec<isize> {
    input.lines().flat_map(isize::from_str).collect()
}

const BUF_SIZE: usize = 25;

pub fn solve_part1(input: &[isize]) -> isize {
    let mut buf = Vec::with_capacity(BUF_SIZE);

    for &num in input.iter().take(BUF_SIZE) {
        buf.push(num)
    }

    for &num in input.iter().skip(BUF_SIZE) {
        let mut is_ok = false;

        'outer: for &a in buf.iter() {
            for &b in buf.iter() {
                if a + b == num {
                    is_ok = true;
                    break 'outer;
                }
            }
        }

        buf.remove(0);
        buf.push(num);

        if !is_ok {
            return num;
        }
    }

    0
}

pub fn solve_part2(input: &[isize]) -> isize {
    let part1_solution = solve_part1(input);

    for left in 0..input.len() {
        for right in left..input.len() {
            let slice = &input[left..=right];
            if slice.iter().sum::<isize>() == part1_solution {
                return slice.iter().min().expect("non-empty")
                    + slice.iter().max().expect("non-empty");
            }
        }
    }

    0
}
