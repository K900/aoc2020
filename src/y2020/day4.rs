use std::collections::HashMap;
use std::str::FromStr;

pub fn input_generator(input: &str) -> Vec<HashMap<String, String>> {
    let mut items = Vec::new();

    for block in input.split("\n\n") {
        let mut map = HashMap::new();
        for field in block.split_ascii_whitespace() {
            let (k, v) = field.split_once(':').expect("Failed to split by :");
            map.insert(k.into(), v.into());
        }
        items.push(map)
    }

    items
}

fn has_required_fields(passport: &HashMap<String, String>) -> bool {
    for key in ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"].iter() {
        if !passport.contains_key(&key.to_string()) {
            return false;
        }
    }
    true
}

fn byr_valid(passport: &HashMap<String, String>) -> bool {
    if let Some(byr) = passport.get("byr") {
        let byr = u32::from_str(byr).unwrap_or(0);
        return (1920..=2002).contains(&byr);
    }

    true
}

fn iyr_valid(passport: &HashMap<String, String>) -> bool {
    if let Some(iyr) = passport.get("iyr") {
        let iyr = u32::from_str(iyr).unwrap_or(0);
        return (2010..=2020).contains(&iyr);
    }

    true
}

fn eyr_valid(passport: &HashMap<String, String>) -> bool {
    if let Some(eyr) = passport.get("eyr") {
        let eyr = u32::from_str(eyr).unwrap_or(0);
        return (2020..=2030).contains(&eyr);
    }

    true
}

fn hgt_valid(passport: &HashMap<String, String>) -> bool {
    if let Some(hgt) = passport.get("hgt") {
        if let Some(h_cm) = hgt.strip_suffix("cm") {
            let h_cm = u32::from_str(h_cm).unwrap_or(0);
            return (150..=193).contains(&h_cm);
        }

        if let Some(h_in) = hgt.strip_suffix("in") {
            let h_in = u32::from_str(h_in).unwrap_or(0);
            return (59..=76).contains(&h_in);
        }

        return false;
    }

    true
}

fn hcl_valid(passport: &HashMap<String, String>) -> bool {
    if let Some(hcl) = passport.get("hcl") {
        if let Some(hcl_hex) = hcl.strip_prefix('#') {
            return hcl_hex.chars().all(|x| x.is_ascii_hexdigit()) && hcl_hex.len() == 6;
        }

        return false;
    }

    true
}

fn ecl_valid(passport: &HashMap<String, String>) -> bool {
    if let Some(ecl) = passport.get("ecl") {
        return ecl == "amb"
            || ecl == "blu"
            || ecl == "brn"
            || ecl == "gry"
            || ecl == "grn"
            || ecl == "hzl"
            || ecl == "oth";
    }

    true
}

fn pid_valid(passport: &HashMap<String, String>) -> bool {
    if let Some(pid) = passport.get("pid") {
        return pid.len() == 9 && pid.chars().all(|x| x.is_ascii_digit());
    }

    true
}

pub fn solve_part1(input: &[HashMap<String, String>]) -> usize {
    input.iter().filter(|&x| has_required_fields(x)).count()
}

pub fn solve_part2(input: &[HashMap<String, String>]) -> usize {
    input
        .iter()
        .filter(|&x| has_required_fields(x))
        .filter(|&x| byr_valid(x))
        .filter(|&x| iyr_valid(x))
        .filter(|&x| eyr_valid(x))
        .filter(|&x| hgt_valid(x))
        .filter(|&x| hcl_valid(x))
        .filter(|&x| ecl_valid(x))
        .filter(|&x| pid_valid(x))
        .count()
}
