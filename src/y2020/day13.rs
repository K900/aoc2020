use std::str::FromStr;

pub fn input_generator(input: &str) -> (usize, Vec<(usize, usize)>) {
    let (first, rest) = input.split_once('\n').expect("invalid input");
    let timestamp = usize::from_str(first).expect("invalid input");
    (
        timestamp,
        rest.split(',')
            .enumerate()
            .filter_map(|(i, x)| match usize::from_str(x) {
                Ok(x) => Some((i, x)),
                Err(_) => None,
            })
            .collect(),
    )
}

pub fn solve_part1(input: &(usize, Vec<(usize, usize)>)) -> usize {
    let (timestamp, buses) = input;

    let (bus_id, wait_time) = buses
        .iter()
        .map(|&(_, bus)| {
            (
                bus,
                bus * (*timestamp as f64 / bus as f64).ceil() as usize - timestamp,
            )
        })
        .min_by_key(|&(_, ts)| ts)
        .expect("input is empty");

    bus_id * wait_time
}

pub fn solve_part2(input: &(usize, Vec<(usize, usize)>)) -> usize {
    let (_, buses) = input;

    for (id, bus) in buses.iter() {
        println!("(x + {}) mod {} == 0;", id, bus);
    }

    // I don't want to bother. Just plug this into Wolfram Alpha.

    0
}
