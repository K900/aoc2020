use text_io::scan;

pub struct PasswordRule {
    character: char,
    low: usize,
    high: usize,
}

pub fn input_generator(input: &str) -> Vec<(PasswordRule, String)> {
    input
        .lines()
        .map(|x| {
            let low: usize;
            let high: usize;
            let character: char;
            let password: String;
            scan!(x.bytes() => "{}-{} {}: {}", low, high, character, password);

            (
                PasswordRule {
                    low,
                    high,
                    character,
                },
                password,
            )
        })
        .collect()
}

pub fn solve_part1(input: &[(PasswordRule, String)]) -> usize {
    input
        .iter()
        .filter(|(rule, password)| {
            let nchar = password.chars().filter(|&c| c == rule.character).count();
            (rule.low..=rule.high).contains(&nchar)
        })
        .count()
}

pub fn solve_part2(input: &[(PasswordRule, String)]) -> usize {
    input
        .iter()
        .filter(|(rule, password)| {
            password
                .chars()
                .enumerate()
                .filter(|&(i, x)| (i + 1 == rule.low || i + 1 == rule.high) && x == rule.character)
                .count()
                == 1
        })
        .count()
}
