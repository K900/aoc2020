use std::collections::HashSet;

pub fn input_generator(input: &str) -> Vec<Vec<String>> {
    input
        .split("\n\n")
        .map(|x| x.lines().map(|x| x.into()).collect())
        .collect()
}

pub fn solve_part1(groups: &[Vec<String>]) -> usize {
    groups
        .iter()
        .map(|g| {
            g.iter()
                .flat_map(|s| s.chars())
                .collect::<HashSet<char>>()
                .len()
        })
        .sum()
}

pub fn solve_part2(groups: &[Vec<String>]) -> usize {
    groups
        .iter()
        .map(|g| {
            g.iter()
                .map(|s| s.chars().collect::<HashSet<char>>())
                .reduce(|this, that| this.intersection(&that).cloned().collect::<HashSet<char>>())
                .expect("Group can't be empty")
                .len()
        })
        .sum()
}
