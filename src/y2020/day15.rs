use std::{collections::HashMap, str::FromStr};

pub fn input_generator(input: &str) -> Vec<usize> {
    input.split(',').flat_map(usize::from_str).collect()
}

fn solve(input: &[usize], last_turn: usize) -> usize {
    let mut last_seen = HashMap::new();

    for (idx, &i) in input[..input.len() - 1].iter().enumerate() {
        last_seen.insert(i, idx);
    }

    let mut last_num = *input.last().expect("input must not be empty");

    for i in input.len()..last_turn {
        let next_num = match last_seen.get(&last_num) {
            Some(x) => i - x - 1,
            None => 0,
        };
        last_seen.insert(last_num, i - 1);
        last_num = next_num
    }

    last_num
}

pub fn solve_part1(input: &[usize]) -> usize {
    solve(input, 2020)
}

pub fn solve_part2(input: &[usize]) -> usize {
    // should we be smart here? yes
    // will we be smart here? hahahahahahahahahahahahahahah
    solve(input, 30000000)
}
