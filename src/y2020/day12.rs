use std::{num::ParseIntError, str::FromStr};

#[derive(Debug)]
pub enum Command {
    North(isize),
    South(isize),
    East(isize),
    West(isize),
    Left(isize),
    Right(isize),
    Forward(isize),
}

#[derive(Copy, Clone, Debug)]
enum Facing {
    North,
    South,
    East,
    West,
}

impl Facing {
    fn left_once(&self) -> Self {
        use Facing::*;
        match self {
            North => West,
            South => East,
            East => North,
            West => South,
        }
    }

    fn left(&self, n: isize) -> Self {
        assert_eq!(n % 90, 0);

        let mut current = *self;
        for _ in 0..(n / 90) {
            current = current.left_once()
        }

        current
    }

    fn to_coords(self) -> (isize, isize) {
        match self {
            Facing::North => (0, 1),
            Facing::South => (0, -1),
            Facing::East => (1, 0),
            Facing::West => (-1, 0),
        }
    }
}

#[derive(Debug)]
struct Ship {
    x: isize,
    y: isize,
    facing: Facing,
}

impl Ship {
    fn new() -> Self {
        Ship {
            x: 0,
            y: 0,
            facing: Facing::East,
        }
    }

    fn handle_command(&mut self, cmd: &Command) {
        match *cmd {
            Command::North(value) => self.y += value,
            Command::South(value) => self.y -= value,
            Command::East(value) => self.x += value,
            Command::West(value) => self.x -= value,
            Command::Left(value) => self.facing = self.facing.left(value),
            Command::Right(value) => self.facing = self.facing.left(360 - value),
            Command::Forward(value) => {
                let (dx, dy) = self.facing.to_coords();
                self.x += dx * value;
                self.y += dy * value;
            }
        }
    }
}

#[derive(Debug)]
struct Ship2 {
    x: isize,
    y: isize,
    wx: isize,
    wy: isize,
}

impl Ship2 {
    fn new() -> Self {
        Ship2 {
            x: 0,
            y: 0,
            wx: 10,
            wy: 1,
        }
    }

    fn left_once(&mut self) {
        (self.wx, self.wy) = (-self.wy, self.wx)
    }

    fn left(&mut self, n: isize) {
        assert_eq!(n % 90, 0);

        for _ in 0..(n / 90) {
            self.left_once()
        }
    }

    fn handle_command(&mut self, cmd: &Command) {
        match *cmd {
            Command::North(value) => self.wy += value,
            Command::South(value) => self.wy -= value,
            Command::East(value) => self.wx += value,
            Command::West(value) => self.wx -= value,
            Command::Left(value) => self.left(value),
            Command::Right(value) => self.left(360 - value),
            Command::Forward(value) => {
                self.x += self.wx * value;
                self.y += self.wy * value;
            }
        }
    }
}

impl FromStr for Command {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (first, rest) = s.split_at(1);
        let value = isize::from_str(rest)?;

        Ok(match first {
            "N" => Command::North(value),
            "S" => Command::South(value),
            "E" => Command::East(value),
            "W" => Command::West(value),
            "L" => Command::Left(value),
            "R" => Command::Right(value),
            "F" => Command::Forward(value),
            _ => unreachable!(),
        })
    }
}

pub fn input_generator(input: &str) -> Vec<Command> {
    input.lines().flat_map(Command::from_str).collect()
}

pub fn solve_part1(input: &[Command]) -> isize {
    let mut ship = Ship::new();
    input.iter().for_each(|x| ship.handle_command(x));
    ship.x.abs() + ship.y.abs()
}

pub fn solve_part2(input: &[Command]) -> isize {
    let mut ship = Ship2::new();
    input.iter().for_each(|x| ship.handle_command(x));
    ship.x.abs() + ship.y.abs()
}
