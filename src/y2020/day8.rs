use std::{collections::HashSet, str::FromStr};

#[derive(Debug, Copy, Clone)]
pub enum Instruction {
    Nop(isize),
    Jmp(isize),
    Acc(isize),
}

pub fn input_generator(input: &str) -> Vec<Instruction> {
    let mut instrs = Vec::new();

    for line in input.lines() {
        let (opcode, value) = line.split_once(' ').expect("failed to parse line");
        let value = isize::from_str(value).expect("failed to parse value");
        let instruction = match opcode {
            "nop" => Instruction::Nop(value),
            "jmp" => Instruction::Jmp(value),
            "acc" => Instruction::Acc(value),
            _ => unimplemented!(),
        };

        instrs.push(instruction);
    }

    instrs
}

#[derive(Debug)]
pub enum ExitReason {
    Looped(isize),
    Terminated(isize),
}

fn eval(input: &[Instruction]) -> ExitReason {
    let mut pc = 0isize;
    let mut acc = 0;

    let mut seen = HashSet::new();

    loop {
        if seen.contains(&pc) {
            return ExitReason::Looped(acc);
        }

        if (pc as usize) == input.len() {
            return ExitReason::Terminated(acc);
        }

        seen.insert(pc);

        match input[pc as usize] {
            Instruction::Nop(_) => pc += 1,
            Instruction::Jmp(v) => pc += v,
            Instruction::Acc(v) => {
                acc += v;
                pc += 1
            }
        }
    }
}

pub fn solve_part1(input: &[Instruction]) -> isize {
    if let ExitReason::Looped(acc) = eval(input) {
        return acc;
    }

    unreachable!()
}

pub fn solve_part2(input: &[Instruction]) -> isize {
    for (i, instr) in input.iter().enumerate() {
        let new_instr = match instr {
            Instruction::Nop(v) => Instruction::Jmp(*v),
            Instruction::Jmp(v) => Instruction::Nop(*v),
            Instruction::Acc(_) => continue,
        };

        let mut new_data = input.to_owned();
        new_data[i] = new_instr;

        if let ExitReason::Terminated(acc) = eval(&new_data) {
            return acc;
        }
    }

    unreachable!()
}
