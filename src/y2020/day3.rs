use std::ops::Index;

pub struct Map {
    data: Vec<bool>,
    width: usize,
    height: usize,
}

impl Index<(usize, usize)> for Map {
    type Output = bool;

    fn index(&self, index: (usize, usize)) -> &Self::Output {
        let (x, y) = index;
        &self.data[y * self.width + x % self.width]
    }
}

pub fn input_generator(input: &str) -> Map {
    let mut bits = Vec::new();
    let mut height = 0;
    let mut width = 0;

    for line in input.lines() {
        width = 0;
        for char in line.chars() {
            width += 1;
            bits.push(match char {
                '.' => false,
                '#' => true,
                _ => panic!("Unexpected character!"),
            })
        }
        height += 1;
    }

    Map {
        data: bits,
        width,
        height,
    }
}

fn solve(input: &Map, dx: usize, dy: usize) -> usize {
    let mut x = 0;
    let mut y = 0;
    let mut trees = 0;

    while y < input.height {
        if input[(x, y)] {
            trees += 1;
        }
        x += dx;
        y += dy;
    }

    trees
}

pub fn solve_part1(input: &Map) -> usize {
    solve(input, 3, 1)
}

pub fn solve_part2(input: &Map) -> usize {
    solve(input, 1, 1)
        * solve(input, 3, 1)
        * solve(input, 5, 1)
        * solve(input, 7, 1)
        * solve(input, 1, 2)
}
