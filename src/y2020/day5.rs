pub fn input_generator(input: &str) -> Vec<String> {
    input.lines().map(|x| x.into()).collect()
}

fn to_seat_id(bsp: &str) -> usize {
    let mut low_row = 0;
    let mut high_row = 128;
    let mut low_col = 0;
    let mut high_col = 8;

    for char in bsp.chars() {
        match char {
            'F' => high_row = (low_row + high_row) / 2,
            'B' => low_row = (low_row + high_row) / 2,
            'L' => high_col = (low_col + high_col) / 2,
            'R' => low_col = (low_col + high_col) / 2,
            _ => unimplemented!(),
        }
    }

    low_row * 8 + low_col
}

pub fn solve_part1(input: &[String]) -> usize {
    input
        .iter()
        .map(|x| to_seat_id(x))
        .max()
        .expect("Input is empty")
}

pub fn solve_part2(input: &[String]) -> usize {
    let mut ids: Vec<_> = input.iter().map(|x| to_seat_id(x)).collect();
    ids.sort_unstable();

    for slice in ids.windows(2) {
        let (this, next) = (slice[0], slice[1]);
        if next == this + 2 {
            return this + 1;
        }
    }

    0
}
