use std::{collections::HashMap, fmt::Debug, str::FromStr};

use itertools::Itertools;

pub struct Bitmask {
    mask: Vec<Option<bool>>,
}

impl Debug for Bitmask {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for bit in self.mask.iter().rev() {
            write!(
                f,
                "{}",
                match bit {
                    Some(true) => '1',
                    Some(false) => '0',
                    None => 'X',
                }
            )?;
        }

        Ok(())
    }
}

fn set_bit(value: usize, bit: usize) -> usize {
    value | 1 << bit
}

fn clear_bit(value: usize, bit: usize) -> usize {
    value & !(1 << bit)
}

impl Bitmask {
    fn apply_v1(&self, value: usize) -> usize {
        let mut x = value;
        for (idx, bit) in self.mask.iter().enumerate() {
            match &bit {
                Some(true) => x = set_bit(x, idx),
                Some(false) => x = clear_bit(x, idx),
                None => continue,
            }
        }

        x
    }

    fn apply_v2(&self, value: usize) -> impl Iterator<Item = usize> {
        let mut init_mask = value;
        let mut floating_indices = Vec::new();

        for (idx, bit) in self.mask.iter().enumerate() {
            match &bit {
                Some(true) => init_mask = set_bit(init_mask, idx),
                Some(false) => continue,
                None => floating_indices.push(idx),
            }
        }

        floating_indices
            .iter()
            .map(|_| [false, true].iter())
            .multi_cartesian_product()
            .map(move |vs| {
                let mut cx = init_mask;
                for (&idx, &v) in floating_indices.iter().zip(vs) {
                    if v {
                        cx = set_bit(cx, idx)
                    } else {
                        cx = clear_bit(cx, idx)
                    }
                }

                cx
            })
    }
}

impl FromStr for Bitmask {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut mask = Vec::new();
        for ch in s.chars() {
            mask.push(match ch {
                '1' => Some(true),
                '0' => Some(false),
                'X' => None,
                _ => unreachable!(),
            })
        }

        mask.reverse();
        Ok(Self { mask })
    }
}

#[derive(Debug)]
pub enum Command {
    SetMask(Bitmask),
    Store { dest: usize, value: usize },
}

impl FromStr for Command {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(rest) = s.strip_prefix("mask = ") {
            let bitmask = Bitmask::from_str(rest).expect("invalid format");
            return Ok(Command::SetMask(bitmask));
        }

        if let Some(rest) = s.strip_prefix("mem[") {
            let (this, that) = rest.split_once("] = ").expect("invalid format");
            let dest = usize::from_str(this).expect("invalid format");
            let value = usize::from_str(that).expect("invalid format");
            return Ok(Command::Store { dest, value });
        }

        unreachable!()
    }
}

pub fn input_generator(input: &str) -> Vec<Command> {
    input.lines().flat_map(Command::from_str).collect()
}

pub fn solve_part1(input: &[Command]) -> usize {
    let mut memory = HashMap::new();
    let mut current_mask = &Bitmask { mask: Vec::new() };

    for command in input {
        match command {
            Command::SetMask(m) => current_mask = m,
            Command::Store { value, dest } => {
                memory.insert(*dest, current_mask.apply_v1(*value));
            }
        }
    }

    memory.values().sum()
}

pub fn solve_part2(input: &[Command]) -> usize {
    let mut memory = HashMap::new();
    let mut current_mask = &Bitmask { mask: Vec::new() };

    for command in input {
        match command {
            Command::SetMask(m) => current_mask = m,
            Command::Store { value, dest } => {
                for addr in current_mask.apply_v2(*dest) {
                    memory.insert(addr, *value);
                }
            }
        }
    }

    memory.values().sum()
}
