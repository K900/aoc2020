use std::str::FromStr;

use petgraph::graphmap::DiGraphMap;

pub fn input_generator(input: &str) -> Vec<isize> {
    input.lines().flat_map(isize::from_str).collect()
}

pub fn solve_part1(input: &[isize]) -> isize {
    let mut input = input.to_owned();
    input.push(0);
    input.sort_unstable();

    let mut d1s = 0;
    let mut d3s = 1;

    for [this, that] in input.array_windows::<2>() {
        if that - this == 1 {
            d1s += 1;
        }

        if that - this == 3 {
            d3s += 1;
        }
    }

    d1s * d3s
}

pub fn solve_part2(input: &[isize]) -> usize {
    let mut graph = DiGraphMap::new();

    let mut input = input.to_owned();
    input.push(0);
    input.sort_unstable();
    let last = input.last().expect("input is empty") + 3;
    input.push(last);

    for this in &input {
        for that in &input {
            if this != that && (0..=3).contains(&(that - this)) {
                graph.add_edge(*this, *that, 1usize);
            }
        }
    }

    let mut breakpoints = vec![0];

    for [this, that] in input.array_windows::<2>() {
        if that - this == 3 {
            breakpoints.push(*that);
        }
    }

    breakpoints
        .array_windows::<2>()
        .map(|[this, that]| {
            petgraph::algo::all_simple_paths::<Vec<_>, &DiGraphMap<isize, usize>>(
                &graph, *this, *that, 0, None,
            )
            .count()
        })
        .product()
}
