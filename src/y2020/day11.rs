use std::fmt::Debug;

#[derive(Eq, PartialEq, Copy, Clone, Debug)]
pub enum State {
    Occupied,
    Empty,
    Floor,
}

impl State {
    fn from_char(c: char) -> Self {
        match c {
            '#' => State::Occupied,
            'L' => State::Empty,
            '.' => State::Floor,
            _ => unreachable!(),
        }
    }

    fn to_char(self) -> char {
        match self {
            State::Occupied => '#',
            State::Empty => 'L',
            State::Floor => '.',
        }
    }
}

#[derive(Eq, PartialEq, Clone)]
pub struct Grid {
    data: Vec<Vec<State>>,
}

impl Grid {
    fn is_in_bounds(&self, x: isize, y: isize) -> bool {
        let max_x = self.data[0].len() as isize;
        let max_y = self.data.len() as isize;

        (0..max_x).contains(&x) && (0..max_y).contains(&y)
    }

    fn step(&self, cb: impl Fn(&Grid, usize, usize) -> State) -> Grid {
        let mut new_data = self.data.clone();

        for (y, row) in self.data.iter().enumerate() {
            for (x, _) in row.iter().enumerate() {
                new_data[y][x] = cb(self, x, y);
            }
        }

        Grid { data: new_data }
    }

    fn solve(&self, cb: impl Fn(&Grid, usize, usize) -> State + Copy) -> usize {
        let mut this = self.step(cb);
        loop {
            let next = this.step(cb);
            if this == next {
                break;
            }
            this = next;
        }

        this.data
            .iter()
            .flat_map(|x| x.iter())
            .filter(|&c| *c == State::Occupied)
            .count()
    }
}

impl Debug for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for row in &self.data {
            for state in row {
                write!(f, "{}", state.to_char())?;
            }
            writeln!(f)?;
        }

        Ok(())
    }
}

pub fn input_generator(input: &str) -> Grid {
    let data: Vec<Vec<_>> = input
        .lines()
        .map(|x| x.chars().map(State::from_char).collect())
        .collect();

    Grid { data }
}

fn step_part1(grid: &Grid, x: usize, y: usize) -> State {
    let mut total = 0;

    let x = x as isize;
    let y = y as isize;

    for dx in -1..=1 {
        for dy in -1..=1 {
            if dx == 0 && dy == 0 {
                continue;
            }

            if grid.is_in_bounds(x + dx, y + dy)
                && grid.data[(y + dy) as usize][(x + dx) as usize] == State::Occupied
            {
                total += 1;
            }
        }
    }

    match (grid.data[y as usize][x as usize], total) {
        (State::Occupied, x) if x >= 4 => State::Empty,
        (State::Empty, 0) => State::Occupied,
        (s, _) => s,
    }
}

fn step_part2(grid: &Grid, x: usize, y: usize) -> State {
    let mut total = 0;

    let x = x as isize;
    let y = y as isize;

    for dx in -1..=1 {
        for dy in -1..=1 {
            if dx == 0 && dy == 0 {
                continue;
            }

            let mut cx = x + dx;
            let mut cy = y + dy;
            let mut hit = State::Floor;

            loop {
                if !grid.is_in_bounds(cx, cy) {
                    break;
                }

                let v = grid.data[cy as usize][cx as usize];
                if v != State::Floor {
                    hit = v;
                    break;
                }

                cx += dx;
                cy += dy;
            }

            if hit == State::Occupied {
                total += 1;
            }
        }
    }

    match (grid.data[y as usize][x as usize], total) {
        (State::Occupied, x) if x >= 5 => State::Empty,
        (State::Empty, 0) => State::Occupied,
        (s, _) => s,
    }
}

pub fn solve_part1(input: &Grid) -> usize {
    input.solve(step_part1)
}

pub fn solve_part2(input: &Grid) -> usize {
    input.solve(step_part2)
}
