use hex::decode;
use itertools::Itertools;
use nom::{bits::complete::take, sequence::tuple, IResult, Parser};
use std::fmt::{Debug, Display};

type Bits<'a> = (&'a [u8], usize);
type BResult<'a, T> = IResult<Bits<'a>, T>;

#[derive(Debug)]
pub enum Operator {
    Sum,
    Product,
    Min,
    Max,
    Greater,
    Lesser,
    Equal,
}

pub enum PacketData {
    Literal(usize),
    Operation(Operator, Vec<Packet>),
}

pub struct Packet {
    version: usize,
    data: PacketData,
}

impl Packet {
    fn version_sum(&self) -> usize {
        self.version
            + match &self.data {
                PacketData::Literal(_) => 0usize,
                PacketData::Operation(_, ps) => ps.iter().map(Packet::version_sum).sum(),
            }
    }

    fn eval(&self) -> usize {
        match &self.data {
            PacketData::Literal(x) => *x,
            PacketData::Operation(op, args) => {
                let results: Vec<_> = args.iter().map(Packet::eval).collect();

                match op {
                    Operator::Sum => results.iter().sum(),
                    Operator::Product => results.iter().product(),
                    Operator::Min => *results.iter().min().expect("args empty"),
                    Operator::Max => *results.iter().max().expect("args empty"),
                    Operator::Greater => (results[0] > results[1]) as usize,
                    Operator::Lesser => (results[0] < results[1]) as usize,
                    Operator::Equal => (results[0] == results[1]) as usize,
                }
            }
        }
    }
}

impl Debug for Packet {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.data {
            PacketData::Literal(x) => write!(f, "{} [{}]", x, self.version)?,
            PacketData::Operation(op, args) => {
                write!(
                    f,
                    "{} [{}]",
                    match op {
                        Operator::Sum => "+",
                        Operator::Product => "*",
                        Operator::Min => "min",
                        Operator::Max => "max",
                        Operator::Greater => ">",
                        Operator::Lesser => "<",
                        Operator::Equal => "=",
                    },
                    self.version
                )?;
                writeln!(f)?;

                for arg in args {
                    writeln!(f, "|-- {}", format!("{:?}", arg).lines().join("\n|   "))?;
                }
            }
        }

        Ok(())
    }
}

impl Display for Packet {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.data {
            PacketData::Literal(x) => write!(f, "{}", x)?,
            PacketData::Operation(op, args) => {
                write!(
                    f,
                    "({} ",
                    match op {
                        Operator::Sum => "+",
                        Operator::Product => "*",
                        Operator::Min => "min",
                        Operator::Max => "max",
                        Operator::Greater => ">",
                        Operator::Lesser => "<",
                        Operator::Equal => "=",
                    },
                )?;

                for arg in args {
                    write!(f, "{} ", arg)?;
                }

                write!(f, ")")?;
            }
        }

        Ok(())
    }
}

fn parse_literal(input: Bits) -> BResult<usize> {
    let mut rest = input;
    let mut result = 0;
    let mut v: usize;
    loop {
        (rest, v) = take(5usize).parse(rest)?;

        let significant = v & 0b01111;
        result = (result << 4) + significant;

        if v & 0b10000 == 0 {
            return Ok((rest, result));
        }
    }
}

fn parse_type0_args(input: Bits) -> BResult<Vec<Packet>> {
    let (mut rest, subp_len): (Bits, usize) = take(15usize).parse(input)?;
    let mut result = vec![];

    fn blen(input: Bits) -> usize {
        input.0.len() * 8 - input.1
    }

    let start_len = blen(rest);

    while start_len - blen(rest) < subp_len {
        let subp_res = parse_packet(rest)?;
        rest = subp_res.0;
        result.push(subp_res.1);
    }

    Ok((rest, result))
}

fn parse_type1_args(input: Bits) -> BResult<Vec<Packet>> {
    let (mut rest, subp_count): (Bits, usize) = take(11usize).parse(input)?;
    let mut result = vec![];

    for _ in 0..subp_count {
        let subp_res = parse_packet(rest)?;
        rest = subp_res.0;
        result.push(subp_res.1);
    }

    Ok((rest, result))
}

fn parse_op_args(input: Bits) -> BResult<Vec<Packet>> {
    let (rest, length_kind): (Bits, u8) = take(1usize).parse(input)?;

    if length_kind == 0 {
        parse_type0_args(rest)
    } else {
        parse_type1_args(rest)
    }
}

fn parse_packet(input: Bits) -> BResult<Packet> {
    let res = tuple((take(3usize), take(3usize))).parse(input)?;

    let (rest, (v, t)): (Bits, (usize, u8)) = res;

    if t == 4 {
        let (rest, value) = parse_literal(rest)?;
        Ok((
            rest,
            Packet {
                version: v,
                data: PacketData::Literal(value),
            },
        ))
    } else {
        let op = match t {
            0 => Operator::Sum,
            1 => Operator::Product,
            2 => Operator::Min,
            3 => Operator::Max,
            5 => Operator::Greater,
            6 => Operator::Lesser,
            7 => Operator::Equal,
            _ => panic!("this should never happen"),
        };

        let (rest, args) = parse_op_args(rest)?;
        Ok((
            rest,
            Packet {
                version: v,
                data: PacketData::Operation(op, args),
            },
        ))
    }
}

pub fn input_generator(input: &str) -> Packet {
    let raw = decode(input).expect("failed to parse hex");
    parse_packet((&raw, 0)).expect("failed to parse packet").1
}

pub fn solve_part1(input: &Packet) -> usize {
    input.version_sum()
}

pub fn solve_part2(input: &Packet) -> usize {
    input.eval()
}
