use itertools::Itertools;

pub struct Target {
    xmin: i64,
    xmax: i64,
    ymin: i64,
    ymax: i64,
}

impl Target {
    fn hit(&self, x: i64, y: i64) -> bool {
        x >= self.xmin && x <= self.xmax && y >= self.ymin && y <= self.ymax
    }

    fn miss(&self, x: i64, y: i64, vx: i64, vy: i64) -> bool {
        (x < self.xmin && vx <= 0) || (x > self.xmax && vx >= 0) || (y < self.ymin && vy <= 0)
    }
}

pub fn input_generator(_: &str) -> Target {
    // meh
    Target {
        xmin: 236,
        xmax: 262,
        ymin: -78,
        ymax: -58,
    }
}

fn yeet(input: &Target) -> (i64, usize) {
    (-1000i64..=1000)
        .cartesian_product(-1000i64..=1000)
        .filter_map(|(vx, vy)| {
            let mut x = 0;
            let mut y = 0;
            let mut vx = vx;
            let mut vy = vy;
            let mut highest = i64::MIN;

            loop {
                x += vx;
                y += vy;

                highest = y.max(highest);

                vy -= 1;
                vx -= vx.signum();

                if input.hit(x, y) {
                    return Some(highest);
                }

                if input.miss(x, y, vx, vy) {
                    return None;
                }
            }
        })
        .fold((i64::MIN, 0), |acc, i| (acc.0.max(i), acc.1 + 1))
}

pub fn solve_part1(input: &Target) -> i64 {
    yeet(input).0
}

pub fn solve_part2(input: &Target) -> usize {
    yeet(input).1
}
