use std::str::FromStr;

use itertools::Itertools;

pub fn input_generator(input: &str) -> Vec<isize> {
    input.lines().flat_map(isize::from_str).collect()
}

pub fn solve_part1(input: &[isize]) -> usize {
    input
        .array_windows::<2>()
        .filter(|[this, that]| that > this)
        .count()
}

pub fn solve_part2(input: &[isize]) -> usize {
    input
        .array_windows::<3>()
        .zip(input.array_windows::<3>().skip(1))
        .filter(|&(these, those)| those.iter().sum::<isize>() > these.iter().sum())
        .count()
}

pub fn solve_part2_sum_first(input: &[isize]) -> usize {
    input
        .array_windows::<3>()
        .map(|x| x.iter().sum::<isize>())
        .tuple_windows()
        .filter(|(this, that)| that > this)
        .count()
}
