trait Brace {
    fn is_open(&self) -> bool;
    fn matching(self) -> Option<char>;
    fn score(self) -> Option<usize>;
    fn completion_score(self) -> Option<usize>;
}

impl Brace for char {
    fn is_open(&self) -> bool {
        matches!(self, '(' | '{' | '[' | '<')
    }

    fn matching(self) -> Option<char> {
        match self {
            '(' => Some(')'),
            '{' => Some('}'),
            '[' => Some(']'),
            '<' => Some('>'),
            _ => None,
        }
    }

    fn score(self) -> Option<usize> {
        match self {
            ')' => Some(3),
            ']' => Some(57),
            '}' => Some(1197),
            '>' => Some(25137),
            _ => None,
        }
    }

    fn completion_score(self) -> Option<usize> {
        match self {
            ')' => Some(1),
            ']' => Some(2),
            '}' => Some(3),
            '>' => Some(4),
            _ => None,
        }
    }
}

enum BraceResult {
    Corrupted(char),
    Incomplete(Vec<char>),
    Ok,
}

fn do_the_thing(x: &str) -> BraceResult {
    let mut stack = vec![];
    for ch in x.chars() {
        if ch.is_open() {
            stack.push(ch)
        } else if stack.last().and_then(|x| x.matching()) == Some(ch) {
            stack.pop();
        } else {
            return BraceResult::Corrupted(ch);
        }
    }

    if stack.is_empty() {
        BraceResult::Ok
    } else {
        BraceResult::Incomplete(stack)
    }
}

pub fn input_generator(input: &str) -> Vec<&str> {
    input.lines().collect()
}

pub fn solve_part1(input: &[&str]) -> usize {
    input
        .iter()
        .filter_map(|&x| {
            if let BraceResult::Corrupted(ch) = do_the_thing(x) {
                Some(ch.score().expect("Weird character"))
            } else {
                None
            }
        })
        .sum()
}

pub fn solve_part2(input: &[&str]) -> usize {
    let mut scores: Vec<_> = input
        .iter()
        .filter_map(|&x| {
            if let BraceResult::Incomplete(st) = do_the_thing(x) {
                Some(st)
            } else {
                None
            }
        })
        .map(|x| {
            x.iter().rfold(0, |acc, ch| {
                acc * 5
                    + ch.matching()
                        .and_then(|x| x.completion_score())
                        .expect("Weird character")
            })
        })
        .collect();

    scores.sort_unstable();
    scores[scores.len() / 2]
}
