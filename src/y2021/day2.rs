use std::str::FromStr;

pub enum Command {
    Forward(isize),
    Down(isize),
    Up(isize),
}

pub fn input_generator(input: &str) -> Vec<Command> {
    input
        .lines()
        .map(|x| {
            let (direction, value) = x.split_once(' ').expect("Failed to parse input");
            let value = isize::from_str(value).expect("Failed to parse value");

            use Command::*;
            match direction {
                "forward" => Forward(value),
                "down" => Down(value),
                "up" => Up(value),
                _ => panic!("Unexpected command value"),
            }
        })
        .collect()
}

pub fn solve_part1(input: &[Command]) -> isize {
    let mut x = 0;
    let mut y = 0;

    use Command::*;
    for cmd in input {
        match cmd {
            Forward(dx) => x += dx,
            Down(dy) => y += dy,
            Up(dy) => y -= dy,
        }
    }

    x * y
}

pub fn solve_part2(input: &[Command]) -> isize {
    let mut x = 0;
    let mut y = 0;
    let mut aim = 0;

    use Command::*;
    for cmd in input {
        match cmd {
            Forward(dx) => {
                x += dx;
                y += dx * aim;
            }
            Down(dy) => aim += dy,
            Up(dy) => aim -= dy,
        }
    }

    x * y
}
