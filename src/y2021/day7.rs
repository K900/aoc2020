use std::str::FromStr;

pub fn input_generator(input: &str) -> Vec<i64> {
    input.split(',').flat_map(i64::from_str).collect()
}

pub fn solve_part1(input: &[i64]) -> i64 {
    input
        .iter()
        .map(|v| input.iter().map(|x| (x - v).abs()).sum())
        .min()
        .expect("Input is empty")
}

pub fn solve_part2(input: &[i64]) -> i64 {
    input
        .iter()
        .map(|v| input.iter().map(|x| (1..=(x - v).abs()).sum::<i64>()).sum())
        .min()
        .expect("Input is empty")
}
