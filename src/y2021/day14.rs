use std::collections::HashMap;

use itertools::{Itertools, MinMaxResult};

#[derive(Debug)]
pub struct Rule {
    ch1: char,
    ch2: char,
    insert: char,
}

pub fn input_generator(input: &str) -> (&str, Vec<Rule>) {
    let mut lines = input.lines();
    let s = lines.next().expect("first line must exist");
    lines.next();

    let rules = lines
        .map(|line| {
            let mut chars = line.chars();
            let ch1 = chars.next().unwrap();
            let ch2 = chars.next().unwrap();
            let insert = chars.nth(4).unwrap();

            Rule { ch1, ch2, insert }
        })
        .collect();

    (s, rules)
}

fn helper(input: &(&str, Vec<Rule>), n: usize) -> usize {
    let (base, rules) = input;

    let mut bigram_counts = HashMap::<_, usize>::new();
    let mut char_counts = HashMap::<_, usize>::new();

    for ch in base.chars() {
        *char_counts.entry(ch).or_default() += 1usize;
    }

    for (ch1, ch2) in base.chars().tuple_windows() {
        *bigram_counts.entry((ch1, ch2)).or_default() += 1usize;
    }

    for _ in 0..n {
        let mut new_counts = bigram_counts.clone();
        for rule in rules {
            let n = *bigram_counts.entry((rule.ch1, rule.ch2)).or_default();
            *char_counts.entry(rule.insert).or_default() += n;
            *new_counts.entry((rule.ch1, rule.ch2)).or_default() -= n;
            *new_counts.entry((rule.ch1, rule.insert)).or_default() += n;
            *new_counts.entry((rule.insert, rule.ch2)).or_default() += n;
        }
        bigram_counts = new_counts;
    }

    if let MinMaxResult::MinMax(min, max) = char_counts.values().minmax() {
        max - min
    } else {
        0
    }
}

pub fn solve_part1(input: &(&str, Vec<Rule>)) -> usize {
    helper(input, 10)
}

pub fn solve_part2(input: &(&str, Vec<Rule>)) -> usize {
    helper(input, 40)
}
