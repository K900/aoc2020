mod day1;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;

aoc_main::main! {
    year 2021;
    day1 : input_generator => solve_part1, solve_part2, solve_part2_sum_first;
    day2 : input_generator => solve_part1, solve_part2;
    day3 : input_generator => solve_part1, solve_part2;
    day4 : input_generator => solve_part1, solve_part2;
    day5 : input_generator => solve_part1, solve_part2;
    day6 : input_generator => solve_part1, solve_part2;
    day7 : input_generator => solve_part1, solve_part2;
    day8 : input_generator => solve_part1, solve_part2;
    day9 : input_generator => solve_part1, solve_part2;
    day10 : input_generator => solve_part1, solve_part2;
    day11 : input_generator => solve_part1, solve_part2;
    day12 : input_generator => solve_part1, solve_part2;
    day13 : input_generator => solve_part1, solve_part2;
    day14 : input_generator => solve_part1, solve_part2;
    day15 : input_generator => solve_part1, solve_part2;
    day16 : input_generator => solve_part1, solve_part2;
    day17 : input_generator => solve_part1, solve_part2;
    day18 : input_generator => solve_part1, solve_part2;
}

pub fn run() {
    main()
}
