use std::str::FromStr;

const BOARD_SIZE: usize = 5;

#[derive(Clone, Debug)]
pub struct Board {
    numbers: Vec<u64>,
    marks: Vec<bool>,
}

impl Board {
    fn new(numbers: Vec<u64>) -> Board {
        let n = numbers.len();
        Board {
            numbers,
            marks: vec![false; n],
        }
    }

    fn mark(&mut self, n: u64) {
        if let Some(n) = self.numbers.iter().position(|&x| x == n) {
            self.marks[n] = true;
        }
    }

    fn is_marked(&self, x: usize, y: usize) -> bool {
        self.marks[y * BOARD_SIZE + x]
    }

    fn sum_of_unmarked(&self) -> u64 {
        return self
            .numbers
            .iter()
            .zip(self.marks.iter())
            .filter(|&(_, m)| !m)
            .map(|(n, _)| n)
            .sum();
    }

    fn is_winning(&self) -> bool {
        // check columns
        for x in 0..BOARD_SIZE {
            if (0..BOARD_SIZE).map(|y| self.is_marked(x, y)).all(|x| x) {
                return true;
            }
        }

        // check rows
        for y in 0..BOARD_SIZE {
            if (0..BOARD_SIZE).map(|x| self.is_marked(x, y)).all(|x| x) {
                return true;
            }
        }

        false
    }
}

pub fn input_generator(input: &str) -> (Vec<u64>, Vec<Board>) {
    let mut it = input.lines();
    let numbers = it
        .next()
        .expect("Must have one line")
        .split(',')
        .flat_map(u64::from_str)
        .collect();

    let mut boards = vec![];

    loop {
        let skip = it.next();
        if skip.is_none() {
            break;
        };

        let numbers: Vec<_> = (0..BOARD_SIZE)
            .flat_map(|_| it.next())
            .flat_map(|x| x.split(' '))
            .flat_map(u64::from_str)
            .collect();

        assert_eq!(numbers.len(), BOARD_SIZE * BOARD_SIZE);

        boards.push(Board::new(numbers))
    }

    (numbers, boards)
}

pub fn solve_part1(input: &(Vec<u64>, Vec<Board>)) -> u64 {
    let (nums, mut boards) = input.clone();

    for num in nums {
        for board in boards.iter_mut() {
            board.mark(num);
            if board.is_winning() {
                return num * board.sum_of_unmarked();
            }
        }
    }

    0
}

pub fn solve_part2(input: &(Vec<u64>, Vec<Board>)) -> u64 {
    let (nums, mut boards) = input.clone();
    let mut won_boards = vec![];
    let mut last_num = 0;

    for num in nums {
        for (idx, board) in boards.iter_mut().enumerate() {
            board.mark(num);
            if board.is_winning() && !won_boards.contains(&idx) {
                won_boards.push(idx);
            }
        }

        if won_boards.len() == boards.len() {
            last_num = num;
            break;
        };
    }

    last_num * boards[*won_boards.last().expect("All boards have won")].sum_of_unmarked()
}
