from dataclasses import dataclass
from itertools import permutations
from typing import List, Set

from z3 import *

@dataclass
class Line:
    inputs: List[str]
    outputs: List[str]


def parse_file():
    for line in open('input/2021/day8.txt'):
        line = [x for x in line.strip().split()]
        yield Line(line[:10], line[11:])


digits = ['abcefg', 'cf', 'acdeg', 'acdfg', 'bcdf', 'abdfg', 'abdefg', 'acf', 'abcdefg', 'abcdfg']
digits_by_length = {i: [x for x in digits if len(x) == i] for i in range(8)}
digit_to_value = {i: str(idx) for idx, i in enumerate(digits)}

letter_to_index = {l: i for i, l in enumerate('abcdefg')}
index_to_letter = {i: l for i, l in enumerate('abcdefg')}

def one_of(sym, vals):
    return Or([sym == v for v in vals])

def sets_equal(syms, vals):
    return And([one_of(sym, vals) for sym in syms])


def solve_line(line):
    s = Solver()

    symbols = {}

    for i in 'abcdefg':
        sym = Int(i)
        symbols[i] = sym
        s.add(sym >= 0)
        s.add(sym <= 6)

    s.add(Distinct(list(symbols.values())))

    for input in line.inputs:
        options = []
        input_symbols = [symbols[i] for i in input]
        for d in digits_by_length[len(input_symbols)]:
            options.append(sets_equal(
                input_symbols,
                [letter_to_index[ch] for ch in d]
            ))

        s.add(Or(options))

    s.check()
    mo = s.model()

    result = {}
    for i in 'abcdefg':
        result[i] = index_to_letter[mo[symbols[i]].as_long()]

    chars = []
    for o in line.outputs:
        key = ''.join(sorted(result[i] for i in o))
        value = digit_to_value[key]
        chars.append(value)

    return int(''.join(chars))

if __name__ == '__main__':
    print(sum(solve_line(line) for line in parse_file()))
