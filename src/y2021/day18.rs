use std::fmt::Debug;

use itertools::Itertools;

#[derive(Debug, Clone)]
pub enum Payload {
    Literal(i64),
    Pair(usize, usize),
}

#[derive(Debug, Clone)]
pub struct Node {
    parent_id: usize,
    payload: Payload,
}

#[derive(Clone)]
pub struct Tree {
    root: usize,
    nodes: Vec<Node>,
}

#[derive(Debug)]
enum Token {
    Open,
    Number(i64),
    Comma,
    Close,
}

impl Tree {
    fn tokenize(line: &str) -> Vec<Token> {
        let mut tokens = vec![];
        let mut chars = line.chars().peekable();

        while let Some(ch) = chars.next() {
            match ch {
                '[' => tokens.push(Token::Open),
                ']' => tokens.push(Token::Close),
                ',' => tokens.push(Token::Comma),
                '0'..='9' => {
                    let mut number = String::new();
                    number.push(ch);
                    if let Some(&next @ '0'..='9') = chars.peek() {
                        number.push(next);
                        chars.next();
                    }
                    tokens.push(Token::Number(number.parse().expect("oh no")));
                }
                _ => panic!("unexpected character"),
            }
        }

        tokens
    }

    fn combine(&mut self, parent: usize, left: usize, right: usize) -> usize {
        let id = self.insert(parent, Payload::Pair(left, right));
        self.nodes[left].parent_id = id;
        self.nodes[right].parent_id = id;
        id
    }

    fn new(line: &str) -> Self {
        let mut s = Tree {
            root: 0,
            nodes: vec![],
        };
        let mut stack = vec![];

        for t in Self::tokenize(line) {
            match t {
                Token::Number(i) => {
                    let payload = Payload::Literal(i);
                    stack.push(s.insert(usize::MAX, payload))
                }
                Token::Close => {
                    let right = stack.pop().expect("oh no");
                    let left = stack.pop().expect("oh no");
                    stack.push(s.combine(usize::MAX, left, right));
                }
                _ => continue,
            }
        }

        s.root = stack.pop().expect("oof");

        s
    }

    fn insert(&mut self, parent: usize, payload: Payload) -> usize {
        let id = self.nodes.len();
        self.nodes.push(Node {
            parent_id: parent,
            payload,
        });
        id
    }

    fn magnitude_impl(&self, id: usize) -> i64 {
        match self.nodes[id].payload {
            Payload::Literal(x) => x,
            Payload::Pair(l, r) => 3 * self.magnitude_impl(l) + 2 * self.magnitude_impl(r),
        }
    }

    fn magnitude(&self) -> i64 {
        self.magnitude_impl(self.root)
    }

    fn find_exploder(&self, id: usize, depth: usize) -> Option<usize> {
        match self.nodes[id].payload {
            Payload::Literal(_) => None,
            Payload::Pair(_, _) if depth >= 4 => Some(id),
            Payload::Pair(l, r) => {
                if let Some(eid) = self.find_exploder(l, depth + 1) {
                    Some(eid)
                } else {
                    self.find_exploder(r, depth + 1)
                }
            }
        }
    }

    fn find_nearest_left(&self, start: usize) -> Option<usize> {
        let mut last;
        let mut here = start;

        loop {
            last = here;
            here = self.nodes[here].parent_id;

            if here == usize::MAX {
                return None;
            }

            let &Payload::Pair(l, _) = &self.nodes[here].payload else {
                panic!("oh no")
            };

            if l != last {
                let mut down = l;
                while let &Payload::Pair(_, r) = &self.nodes[down].payload {
                    down = r;
                }

                return Some(down);
            }
        }
    }

    fn find_nearest_right(&self, start: usize) -> Option<usize> {
        let mut last;
        let mut here = start;

        loop {
            last = here;
            here = self.nodes[here].parent_id;

            if here == usize::MAX {
                return None;
            }

            let &Payload::Pair(_, r) = &self.nodes[here].payload else {
                panic!("oh no")
            };

            if r != last {
                let mut down = r;
                while let &Payload::Pair(l, _) = &self.nodes[down].payload {
                    down = l;
                }

                return Some(down);
            }
        }
    }

    fn try_explode(&mut self) -> bool {
        if let Some(exploder) = self.find_exploder(self.root, 0) {
            let &Payload::Pair(l, r) = &self.nodes[exploder].payload else {
                panic!("oh no")
            };
            let Payload::Literal(lv) = &self.nodes[l].payload else {
                panic!("oh no")
            };
            let Payload::Literal(rv) = &self.nodes[r].payload else {
                panic!("oh no")
            };

            let lv = *lv;
            let rv = *rv;

            if let Some(ln) = self.find_nearest_left(exploder) {
                let Payload::Literal(lnv) = &mut self.nodes[ln].payload else {
                    panic!("oh no")
                };
                *lnv += lv;
            }

            if let Some(rn) = self.find_nearest_right(exploder) {
                let Payload::Literal(rnv) = &mut self.nodes[rn].payload else {
                    panic!("oh no")
                };
                *rnv += rv;
            }

            self.nodes[exploder].payload = Payload::Literal(0);

            true
        } else {
            false
        }
    }

    fn try_split(&mut self) -> bool {
        let mut queue = vec![self.root];

        while let Some(id) = queue.pop() {
            match self.nodes[id].payload {
                Payload::Literal(x) if x >= 10 => {
                    let half = x as f64 / 2.0;

                    self.nodes[id].payload = Payload::Pair(
                        self.insert(id, Payload::Literal(half.floor() as i64)),
                        self.insert(id, Payload::Literal(half.ceil() as i64)),
                    );

                    return true;
                }
                Payload::Pair(l, r) => {
                    queue.push(r);
                    queue.push(l);
                }
                _ => continue,
            }
        }

        false
    }

    fn reduce(mut self) -> Self {
        loop {
            if self.try_explode() {
                continue;
            }
            if self.try_split() {
                continue;
            }
            return self;
        }
    }

    fn add(self, other: Tree) -> Tree {
        let mut other = other.reduce();

        let offset = self.nodes.len();

        for node in other.nodes.iter_mut() {
            if node.parent_id != usize::MAX {
                node.parent_id += offset;
            }
            if let Payload::Pair(l, r) = &mut node.payload {
                *l += offset;
                *r += offset;
            }
        }

        let mut new_nodes = self.nodes;
        new_nodes.extend(other.nodes);

        let mut new_tree = Tree {
            root: usize::MAX,
            nodes: new_nodes,
        };

        new_tree.root = new_tree.combine(usize::MAX, self.root, other.root + offset);

        new_tree.reduce()
    }

    fn subtree_equal(&self, idx: usize, other: &Self, other_idx: usize) -> bool {
        match (&self.nodes[idx].payload, &other.nodes[other_idx].payload) {
            (&Payload::Literal(x), &Payload::Literal(y)) if x == y => true,
            (&Payload::Pair(our_l, our_r), &Payload::Pair(other_l, other_r)) => {
                self.subtree_equal(our_l, other, other_l)
                    && self.subtree_equal(our_r, other, other_r)
            }
            _ => false,
        }
    }

    fn debug_subtree(
        &self,
        f: &mut std::fmt::Formatter<'_>,
        idx: usize,
        offset: usize,
    ) -> std::fmt::Result {
        write!(f, "[{:<3}] {}", idx, " ".repeat(offset))?;
        match self.nodes[idx].payload {
            Payload::Literal(x) => writeln!(f, "{}", x)?,
            Payload::Pair(l, r) => {
                writeln!(f, "*")?;
                self.debug_subtree(f, l, offset + 2)?;
                self.debug_subtree(f, r, offset + 2)?;
            }
        };

        Ok(())
    }
}

impl Debug for Tree {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.debug_subtree(f, self.root, 0)
    }
}

impl PartialEq for Tree {
    fn eq(&self, other: &Self) -> bool {
        self.subtree_equal(self.root, other, other.root)
    }
}

impl Eq for Tree {}

pub fn input_generator(input: &str) -> Vec<Tree> {
    input.lines().map(Tree::new).collect()
}

pub fn solve_part1(input: &[Tree]) -> i64 {
    input
        .iter()
        .cloned()
        .reduce(|acc, next| acc.add(next.reduce()).reduce())
        .expect("not empty")
        .magnitude()
}

pub fn solve_part2(input: &[Tree]) -> i64 {
    input
        .iter()
        .cloned()
        .cartesian_product(input.iter().cloned())
        .filter(|(a, b)| a != b)
        .map(|(a, b)| a.add(b).magnitude())
        .max()
        .expect("oh no")
}

#[test]
fn test_magnitude() {
    assert_eq!(Tree::new("[[1,2],[[3,4],5]]").magnitude(), 143);
    assert_eq!(
        Tree::new("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]").magnitude(),
        1384
    );
    assert_eq!(Tree::new("[[[[1,1],[2,2]],[3,3]],[4,4]]").magnitude(), 445);
    assert_eq!(Tree::new("[[[[3,0],[5,3]],[4,4]],[5,5]]").magnitude(), 791);
    assert_eq!(Tree::new("[[[[5,0],[7,4]],[5,5]],[6,6]]").magnitude(), 1137);
    assert_eq!(
        Tree::new("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]").magnitude(),
        3488
    );
}

#[test]
fn test_add() {
    let a = Tree::new("[1,2]");
    let b = Tree::new("[3,4]");
    assert_eq!(a.add(b), Tree::new("[[1,2],[3,4]]"))
}

#[test]
fn test_explode() {
    let mut v = Tree::new("[[[[[9,8],1],2],3],4]");
    assert!(v.try_explode());
    assert_eq!(v, Tree::new("[[[[0,9],2],3],4]"));
}

#[test]
fn test_split() {
    let mut v = Tree::new("[[[[0,7],4],[15,[0,13]]],[1,1]]");
    assert!(v.try_split());
    assert_eq!(v, Tree::new("[[[[0,7],4],[[7,8],[0,13]]],[1,1]]"));
    assert!(v.try_split());
    assert_eq!(v, Tree::new("[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]"));
}
