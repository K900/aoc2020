use std::str::FromStr;

pub fn input_generator(input: &str) -> Vec<u8> {
    input.split(',').flat_map(u8::from_str).collect()
}

pub fn helper(input: &[u8], n: usize) -> usize {
    let mut fish = [0; 9];
    for &i in input {
        fish[i as usize] += 1;
    }

    for _ in 0..n {
        fish.rotate_left(1);
        fish[6] += fish[8];
    }

    fish.iter().sum()
}

pub fn solve_part1(input: &[u8]) -> usize {
    helper(input, 80)
}

pub fn solve_part2(input: &[u8]) -> usize {
    helper(input, 256)
}
