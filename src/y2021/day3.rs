use std::{cmp::Ordering, collections::HashSet};

pub fn input_generator(input: &str) -> HashSet<Vec<bool>> {
    input
        .lines()
        .map(|x| x.trim().chars().map(|x| x == '1').collect())
        .collect()
}

fn bits_to_dec(bits: &[bool]) -> usize {
    bits.iter()
        .rev()
        .enumerate()
        .map(|(idx, &bit)| bit as usize * 2usize.pow(idx as u32))
        .sum()
}

fn get_frequencies(input: &HashSet<Vec<bool>>, idx: usize) -> Ordering {
    let (ones, zeroes) = input.iter().fold((0, 0), |(ones, zeroes), el| {
        if el[idx] {
            (ones + 1, zeroes)
        } else {
            (ones, zeroes + 1)
        }
    });

    ones.cmp(&zeroes)
}

pub fn solve_part1(input: &HashSet<Vec<bool>>) -> usize {
    let gamma: Vec<_> = (0..12)
        .map(|idx| get_frequencies(input, idx))
        .map(|x| x == Ordering::Greater)
        .collect();
    let epsilon: Vec<_> = gamma.iter().map(|x| !x).collect();

    bits_to_dec(&gamma) * bits_to_dec(&epsilon)
}

fn part2_impl(input: &HashSet<Vec<bool>>, criteria: impl Fn(Ordering) -> bool) -> usize {
    let mut all_numbers: HashSet<_> = input.clone();

    for idx in 0..12 {
        let criteria_result = criteria(get_frequencies(&all_numbers, idx));
        all_numbers.retain(|x| x[idx] == criteria_result);

        if all_numbers.len() == 1 {
            break;
        }
    }

    bits_to_dec(
        &all_numbers
            .into_iter()
            .next()
            .expect("Set must not be empty"),
    )
}

pub fn solve_part2(input: &HashSet<Vec<bool>>) -> usize {
    let o2_rating = part2_impl(input, |o| match o {
        Ordering::Greater => true, // more ones, return 1
        Ordering::Equal => true,   // equal, return 1
        Ordering::Less => false,   // more zeroes, return 0
    });
    let co2_rating = part2_impl(input, |o| match o {
        Ordering::Greater => false, // less zeroes, return 0
        Ordering::Equal => false,   // equal, return 0
        Ordering::Less => true,     // less ones, return 1
    });

    o2_rating * co2_rating
}

#[test]
fn test_bits_to_dec() {
    assert_eq!(bits_to_dec(&[true, false, true, true, false]), 22);
    assert_eq!(bits_to_dec(&[false, true, false, false, true]), 9);
}
