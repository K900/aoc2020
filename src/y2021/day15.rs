use petgraph::{algo::dijkstra, graphmap::UnGraphMap};

use crate::utils::grid::Grid;

type G = Grid<u32, 100, 100>;

pub fn input_generator(input: &str) -> G {
    G::from_digit_grid(input)
}

fn solve<const X: usize, const Y: usize>(input: &Grid<u32, X, Y>) -> u32 {
    let mut graph = UnGraphMap::<(usize, usize), ()>::new();

    for (x, y) in input.all_indices() {
        graph.add_node((x, y));
        for (nx, ny) in input.neighbor_indices_cardinal(x, y) {
            graph.add_node((nx, ny));
            graph.add_edge((x, y), (nx, ny), ());
        }
    }

    let result = dijkstra(&graph, (0, 0), Some((X - 1, Y - 1)), |(_, to, _)| input[to]);

    result[&(X - 1, Y - 1)]
}

pub fn solve_part1(input: &G) -> u32 {
    solve(input)
}

pub fn solve_part2(input: &G) -> u32 {
    let mut bigger_grid: Grid<u32, 500, 500> = Grid::default();

    for (dvy, dx) in [0, 100, 200, 300, 400].into_iter().enumerate() {
        for (dvx, dy) in [0, 100, 200, 300, 400].into_iter().enumerate() {
            for (x, y) in input.all_indices() {
                let new_risk = input[(x, y)] + dvx as u32 + dvy as u32;
                bigger_grid[(x + dx, y + dy)] = if new_risk > 9 { new_risk - 9 } else { new_risk };
            }
        }
    }

    solve(&bigger_grid)
}
