use std::collections::HashMap;

use itertools::Itertools;

use crate::utils::grid::Grid;

type G = Grid<u32, 100, 100>;

pub fn input_generator(input: &str) -> G {
    G::from_digit_grid(input)
}

fn lows(input: &G) -> Vec<(usize, usize)> {
    input
        .all_indices()
        .filter(|&(x, y)| {
            input
                .neighbor_indices_cardinal(x, y)
                .into_iter()
                .all(|(nx, ny)| input[(x, y)] < input[(nx, ny)])
        })
        .collect()
}

pub fn solve_part1(input: &G) -> u32 {
    lows(input)
        .into_iter()
        .map(|(x, y)| input[(x, y)] + 1)
        .sum()
}

pub fn solve_part2(input: &G) -> usize {
    let mut new_map = *input;
    let mut current_basin = 9;

    for (x, y) in lows(input) {
        current_basin += 1;

        let mut queue = vec![(x, y)];

        while let Some((nx, ny)) = queue.pop() {
            new_map[(nx, ny)] = current_basin;
            queue.extend(
                new_map
                    .neighbor_indices_cardinal(nx, ny)
                    .into_iter()
                    .filter(|&(x, y)| new_map[(x, y)] < 9),
            )
        }
    }

    let mut counts: HashMap<u32, usize> = HashMap::new();
    for (x, y) in new_map.all_indices() {
        let item = new_map[(x, y)];
        if item > 9 {
            *counts.entry(item).or_default() += 1;
        }
    }

    counts.values().sorted().rev().take(3).product()
}
