use std::collections::HashMap;

use petgraph::graphmap::UnGraphMap;

pub fn input_generator(input: &str) -> UnGraphMap<&str, ()> {
    let mut graph = UnGraphMap::new();

    for line in input.lines() {
        let (from, to) = line.split_once('-').expect("line must contain a dash");

        graph.add_edge(from, to, ());
    }

    graph
}

fn search(
    here: &str,
    graph: &UnGraphMap<&str, ()>,
    seen: &HashMap<&str, usize>,
    predicate: impl Fn(&str, &HashMap<&str, usize>) -> bool + Copy,
) -> usize {
    if here == "end" {
        return 1;
    }

    let mut paths = 0;
    let mut seen = seen.clone();

    if here == here.to_ascii_lowercase() {
        *seen.entry(here).or_insert(0) += 1;
    }

    for edge in graph.neighbors(here) {
        if edge == "start" {
            continue;
        }
        if predicate(edge, &seen) {
            paths += search(edge, graph, &seen, predicate);
        }
    }

    paths
}

pub fn solve_part1(graph: &UnGraphMap<&str, ()>) -> usize {
    search("start", graph, &HashMap::new(), |edge, seen| {
        !seen.contains_key(edge)
    })
}

pub fn solve_part2(graph: &UnGraphMap<&str, ()>) -> usize {
    search("start", graph, &HashMap::new(), |edge, seen| {
        !seen.contains_key(edge) // never seen before
        || (seen.get(edge) == Some(&1) && seen.values().all(|&x| x < 2)) // first time going through a small cave twice
    })
}
