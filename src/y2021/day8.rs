use std::collections::HashSet;

type Letters = HashSet<char>;

#[derive(Debug)]
pub struct Line {
    // input: Vec<Letters>,
    output: Vec<Letters>,
}

pub fn input_generator(input: &str) -> Vec<Line> {
    input
        .lines()
        .map(|x| {
            let parts: Vec<HashSet<char>> = x.split(' ').map(|x| x.chars().collect()).collect();

            Line {
                // input: parts[0..10].into(),
                output: parts[11..15].into(),
            }
        })
        .collect()
}

pub fn solve_part1(input: &[Line]) -> usize {
    input
        .iter()
        .map(|l| {
            l.output
                .iter()
                .filter(|&x| matches!(x.len(), 2 | 3 | 4 | 7))
                .count()
        })
        .sum()
}

pub fn solve_part2(_: &[Line]) -> usize {
    0
}
