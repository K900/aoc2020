use std::fmt::Debug;
use std::ops::Add;

use nom::bytes::complete::tag;
use nom::character::complete::{digit1, line_ending};
use nom::multi::separated_list0;
use nom::sequence::tuple;
use nom::{IResult, Parser};

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Point {
    x: isize,
    y: isize,
}

impl Debug for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{}, {}]", self.x, self.y)
    }
}

impl Add for Point {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

#[derive(Clone, Copy)]
pub struct Line {
    start: Point,
    end: Point,
}

impl Debug for Line {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?} -> {:?}", self.start, self.end)
    }
}

impl Line {
    fn points(&self) -> impl Iterator<Item = Point> {
        PointsIterator::new(self)
    }

    fn dx(&self) -> isize {
        self.end.x - self.start.x
    }

    fn dy(&self) -> isize {
        self.end.y - self.start.y
    }

    fn direction(&self) -> Point {
        Point {
            x: self.dx().signum(),
            y: self.dy().signum(),
        }
    }
}

struct PointsIterator {
    current: Point,
    end: Point,
    delta: Point,
    done: bool,
}

impl PointsIterator {
    fn new(l: &Line) -> Self {
        Self {
            current: l.start,
            end: l.end,
            delta: l.direction(),
            done: false,
        }
    }
}

impl Iterator for PointsIterator {
    type Item = Point;

    fn next(&mut self) -> Option<Self::Item> {
        if self.done {
            return None;
        }

        if self.current == self.end {
            self.done = true;
        }

        let result = Some(self.current);

        self.current = self.current + self.delta;

        result
    }
}

fn parse_point(stream: &str) -> IResult<&str, Point> {
    tuple((digit1, tag(","), digit1))
        .map(|(x, _, y): (&str, _, &str)| Point {
            x: x.parse().expect("Failed to parse x"),
            y: y.parse().expect("Failed to parse y"),
        })
        .parse(stream)
}

fn parse_line(stream: &str) -> IResult<&str, Line> {
    tuple((parse_point, tag(" -> "), parse_point))
        .map(|(start, _, end)| Line { start, end })
        .parse(stream)
}

pub fn input_generator(input: &str) -> Vec<Line> {
    separated_list0(line_ending, parse_line)
        .parse(input)
        .expect("Failed to parse input")
        .1
}

fn helper(input: &[Line], predicate: impl Fn(&&Line) -> bool) -> usize {
    let max_x = input
        .iter()
        .flat_map(|l| [l.start.x, l.end.x])
        .max()
        .expect("Input is empty");
    let max_y = input
        .iter()
        .flat_map(|l| [l.start.y, l.end.y])
        .max()
        .expect("Input is empty");

    let mut grid = vec![vec![0; max_x as usize + 1]; max_y as usize + 1];

    for line in input.iter().filter(predicate) {
        for point in line.points() {
            grid[point.y as usize][point.x as usize] += 1;
        }
    }

    grid.into_iter().flatten().filter(|&x| x >= 2).count()
}

pub fn solve_part1(input: &[Line]) -> usize {
    helper(input, |&l| l.dx() == 0 || l.dy() == 0)
}

pub fn solve_part2(input: &[Line]) -> usize {
    helper(input, |&l| {
        l.dx() == 0 || l.dy() == 0 || l.dx().abs() == l.dy().abs()
    })
}
