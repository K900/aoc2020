use itertools::Itertools;

#[derive(Debug)]
pub enum Fold {
    X(usize),
    Y(usize),
}

pub fn input_generator(input: &str) -> (Vec<(usize, usize)>, Vec<Fold>) {
    let mut dots = vec![];
    let mut folds = vec![];

    for line in input.lines() {
        if let Some(part) = line.strip_prefix("fold along ") {
            let (coord, value) = part.split_once('=').expect("fold line must contain =");
            let value = value.parse().expect("failed to parse fold value");

            let fold = match coord {
                "x" => Fold::X(value),
                "y" => Fold::Y(value),
                _ => panic!("unknown fold type"),
            };

            folds.push(fold);
        } else if let Some((x, y)) = line.split_once(',') {
            let x = x.parse().expect("failed to parse x coord");
            let y = y.parse().expect("failed to parse y coord");
            dots.push((x, y))
        }
    }

    (dots, folds)
}

fn helper(dots: &[(usize, usize)], folds: &[Fold]) -> Vec<(usize, usize)> {
    let mut dots: Vec<_> = dots.into();

    for fold in folds.iter() {
        for dot in dots.iter_mut() {
            match *fold {
                Fold::X(x) => {
                    if dot.0 > x {
                        *dot = (2 * x - dot.0, dot.1);
                    }
                }
                Fold::Y(y) => {
                    if dot.1 > y {
                        *dot = (dot.0, 2 * y - dot.1);
                    }
                }
            }
        }
    }

    dots
}

pub fn solve_part1(input: &(Vec<(usize, usize)>, Vec<Fold>)) -> usize {
    let (dots, folds) = input;
    helper(dots, &folds[0..1]).iter().unique().count()
}

pub fn solve_part2(input: &(Vec<(usize, usize)>, Vec<Fold>)) -> usize {
    let (dots, folds) = input;
    let dots = helper(dots, folds);

    let xmax = *dots.iter().map(|(x, _)| x).max().expect("dots empty");
    let ymax = *dots.iter().map(|(_, y)| y).max().expect("dots empty");

    for y in 0..=ymax {
        for x in 0..=xmax {
            if dots.contains(&(x, y)) {
                print!("█");
            } else {
                print!(" ");
            }
        }
        println!();
    }

    0
}
