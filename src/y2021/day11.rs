use crate::utils::grid::Grid;

type G = Grid<u32, 10, 10>;

pub fn input_generator(input: &str) -> G {
    G::from_digit_grid(input)
}

fn step(grid: &mut G) -> usize {
    for (x, y) in grid.all_indices() {
        grid[(x, y)] += 1;
    }

    let mut total_flashes = 0;
    loop {
        let mut flashes = 0;

        for (x, y) in grid.all_indices() {
            if grid[(x, y)] > 9 && grid[(x, y)] < u32::MAX {
                for (nx, ny) in grid.neighbor_indices_corners(x, y) {
                    grid[(nx, ny)] = grid[(nx, ny)].saturating_add(1)
                }
                grid[(x, y)] = u32::MAX;
                flashes += 1;
            }
        }

        total_flashes += flashes;
        if flashes == 0 {
            break;
        }
    }

    for (x, y) in grid.all_indices() {
        if grid[(x, y)] == u32::MAX {
            grid[(x, y)] = 0
        }
    }

    total_flashes
}

pub fn solve_part1(input: &G) -> usize {
    let mut grid = *input;
    std::iter::from_fn(|| Some(step(&mut grid))).take(100).sum()
}

pub fn solve_part2(input: &G) -> usize {
    let mut grid = *input;
    std::iter::from_fn(|| Some(step(&mut grid)))
        .position(|flashes| flashes == 100)
        .expect("Empty?")
        + 1
}
