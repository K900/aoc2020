use itertools::Itertools;

pub fn input_generator(input: &str) -> Vec<Vec<isize>> {
    input
        .lines()
        .map(|s| s.split(' ').map(|x| x.parse().unwrap()).collect_vec())
        .collect_vec()
}

fn solve(input: &[Vec<isize>], backwards: bool) -> isize {
    input
        .iter()
        .map(|vs| {
            let mut firsts = vec![];
            let mut here = vs.clone();

            if backwards {
                here.reverse();
            }

            while !here.iter().all(|&x| x == 0) {
                firsts.push(here[0]);
                here = here
                    .iter()
                    .tuple_windows()
                    .map(|(x, y)| y - x)
                    .collect_vec();
            }

            here.push(0);

            if backwards {
                here.reverse();
            }

            firsts.reverse();

            for f in firsts {
                here = [f]
                    .iter()
                    .chain(here.iter())
                    .scan(0, |acc, &x| {
                        *acc += x;
                        Some(*acc)
                    })
                    .collect_vec();
            }

            *here.last().unwrap()
        })
        .sum()
}

pub fn solve_part1(input: &[Vec<isize>]) -> isize {
    solve(input, false)
}

pub fn solve_part2(input: &[Vec<isize>]) -> isize {
    solve(input, true)
}
