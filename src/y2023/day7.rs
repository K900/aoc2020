use itertools::Itertools;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Cards([char; 5]);

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Hand(Cards, usize);

fn score_card(c: char, joker_value: u8) -> u8 {
    match c {
        '2' => 2,
        '3' => 3,
        '4' => 4,
        '5' => 5,
        '6' => 6,
        '7' => 7,
        '8' => 8,
        '9' => 9,
        'T' => 10,
        'J' => joker_value,
        'Q' => 12,
        'K' => 13,
        'A' => 14,
        _ => unreachable!(),
    }
}

pub fn input_generator(input: &str) -> Vec<Hand> {
    input
        .lines()
        .map(|x| {
            let (cards, bid) = x.split_once(' ').unwrap();
            let cards = cards.chars().collect::<Vec<_>>();
            let bid = bid.parse().unwrap();

            Hand(Cards(cards.try_into().unwrap()), bid)
        })
        .collect()
}

pub fn solve_part1(hands: &[Hand]) -> usize {
    hands
        .iter()
        .sorted_by_key(|h| {
            let cards = h.0 .0;
            let counts = cards
                .iter()
                .counts()
                .values()
                .cloned()
                .sorted()
                .collect_vec();

            let score = match counts[..] {
                [1, 1, 1, 1, 1] => 1,
                [1, 1, 1, 2] => 2,
                [1, 2, 2] => 3,
                [1, 1, 3] => 4,
                [2, 3] => 5,
                [1, 4] => 6,
                [5] => 7,
                _ => unreachable!(),
            };

            (score, cards.map(|c| score_card(c, 11)))
        })
        .enumerate()
        .map(|(i, hand)| hand.1 * (i + 1))
        .sum()
}

pub fn solve_part2(hands: &[Hand]) -> usize {
    hands
        .iter()
        .sorted_by_key(|h| {
            let cards = h.0 .0;
            let mut counts = cards.iter().counts();
            let jokers = counts.remove(&'J').unwrap_or(0);
            let counts_v = counts.values().cloned().sorted().collect_vec();

            let score = match (jokers, &counts_v[..]) {
                (0, [1, 1, 1, 1, 1]) => 1,
                (0, [1, 1, 1, 2]) | (1, &[1, 1, 1, 1]) => 2,
                (0, [1, 2, 2]) => 3,
                (0, [1, 1, 3]) | (1, &[1, 1, 2]) | (2, &[1, 1, 1]) => 4,
                (0, [2, 3]) | (1, &[2, 2]) | (2, &[1, 1]) => 5,
                (0, [1, 4]) | (1, &[1, 3]) | (2, &[1, 2]) | (3, &[1, 1]) => 6,
                (0, [5]) | (1, &[4]) | (2, &[3]) | (3, &[2]) | (4, &[1]) | (5, &[]) => 7,
                _ => unreachable!(),
            };

            (score, cards.map(|c| score_card(c, 1)))
        })
        .enumerate()
        .map(|(i, hand)| hand.1 * (i + 1))
        .sum()
}
