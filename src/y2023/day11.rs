use itertools::Itertools;

use crate::utils::grid::Grid;

const SIZE: usize = 140;
pub type G = Grid<bool, SIZE, SIZE>;

pub fn input_generator(input: &str) -> G {
    G::from_grid(input).map(|_, _, &c| c == '#')
}

fn expand_one(x: usize, skips: &[usize], delta: usize) -> usize {
    x + delta * skips.iter().filter(|&&s| s < x).count()
}

fn expanded_galaxies(input: &G, delta: usize) -> Vec<(usize, usize)> {
    let x_skips = (0..SIZE)
        .filter(|&x| (0..SIZE).all(|y| !input[(x, y)]))
        .collect_vec();
    let y_skips = (0..SIZE)
        .filter(|&y| (0..SIZE).all(|x| !input[(x, y)]))
        .collect_vec();

    input
        .all_indices()
        .filter(|&(x, y)| input[(x, y)])
        .map(|(x, y)| {
            (
                expand_one(x, &x_skips, delta),
                expand_one(y, &y_skips, delta),
            )
        })
        .collect_vec()
}

fn manhattan((x1, y1): (usize, usize), (x2, y2): (usize, usize)) -> usize {
    x1.abs_diff(x2) + y1.abs_diff(y2)
}

fn solve(input: &G, delta: usize) -> usize {
    expanded_galaxies(input, delta)
        .into_iter()
        .combinations(2)
        .map(|cs| manhattan(cs[0], cs[1]))
        .sum()
}

pub fn solve_part1(input: &G) -> usize {
    solve(input, 1)
}

pub fn solve_part2(input: &G) -> usize {
    solve(input, 999_999)
}
