use std::collections::HashMap;

use crate::utils::grid::Grid;

const SIZE: usize = 140;
pub type G = Grid<char, SIZE, SIZE>;

pub fn input_generator(input: &str) -> G {
    G::from_grid(input)
}

pub fn solve_part1(input: &G) -> u32 {
    let mut total = 0;

    for row in 0..SIZE {
        let mut num = 0;
        let mut num_good = false;
        for col in 0..SIZE {
            let c = input[(col, row)];

            if let Some(n) = c.to_digit(10) {
                num *= 10;
                num += n;

                for (nx, ny) in input.neighbor_indices_corners(col, row) {
                    let ch = input[(nx, ny)];
                    if !ch.is_ascii_digit() && ch != '.' {
                        num_good = true;
                    }
                }
            } else {
                if num_good {
                    total += num;
                }
                num = 0;
                num_good = false;
            }
        }
        if num_good {
            total += num;
        }
    }

    total
}
pub fn solve_part2(input: &G) -> u32 {
    let mut gears: HashMap<_, Vec<_>> = HashMap::new();

    for row in 0..SIZE {
        let mut num = 0;
        let mut gear = None;
        for col in 0..SIZE {
            let c = input[(col, row)];

            if let Some(n) = c.to_digit(10) {
                num *= 10;
                num += n;

                for (nx, ny) in input.neighbor_indices_corners(col, row) {
                    let ch = input[(nx, ny)];
                    if ch == '*' {
                        gear = Some((nx, ny));
                    }
                }
            } else {
                if let Some((gx, gy)) = gear {
                    gears.entry((gx, gy)).or_default().push(num);
                }
                num = 0;
                gear = None;
            }
        }
        if let Some((gx, gy)) = gear {
            gears.entry((gx, gy)).or_default().push(num);
        }
    }

    gears
        .values()
        .filter(|v| v.len() == 2)
        .map(|v| v.iter().product::<u32>())
        .sum()
}
