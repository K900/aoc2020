use std::collections::HashMap;

use itertools::Itertools;
use nom::{
    bytes::complete::tag,
    character::complete::alpha1,
    character::complete::one_of,
    multi::{many1, separated_list0},
    sequence::tuple,
    IResult, Parser,
};
use num::Integer;

#[derive(Debug)]
pub enum Command {
    Left,
    Right,
}

fn parse_commands(stream: &str) -> IResult<&str, Vec<Command>> {
    tuple((many1(one_of("LR")), tag("\n")))
        .map(|(s, _)| {
            s.into_iter()
                .map(|c| match c {
                    'L' => Command::Left,
                    'R' => Command::Right,
                    _ => unreachable!(),
                })
                .collect_vec()
        })
        .parse(stream)
}

fn parse_item(stream: &str) -> IResult<&str, String> {
    alpha1.map(|x: &str| x.to_owned()).parse(stream)
}

fn parse_map_line(stream: &str) -> IResult<&str, (String, (String, String))> {
    tuple((
        parse_item,
        tag(" = ("),
        parse_item,
        tag(", "),
        parse_item,
        tag(")"),
    ))
    .map(|(f, _, l, _, r, _)| (f, (l, r)))
    .parse(stream)
}

fn parse_full_map(stream: &str) -> IResult<&str, HashMap<String, (String, String)>> {
    separated_list0(tag("\n"), parse_map_line)
        .map(|x| x.into_iter().collect())
        .parse(stream)
}

type Input = (Vec<Command>, HashMap<String, (String, String)>);

fn parse_input(stream: &str) -> IResult<&str, Input> {
    tuple((parse_commands, tag("\n"), parse_full_map))
        .map(|(c, _, m)| (c, m))
        .parse(stream)
}

pub fn input_generator(input: &str) -> Input {
    parse_input(input).unwrap().1
}

fn find_loop((cmds, map): &Input, start: &str, end: impl Fn(&str) -> bool) -> usize {
    let mut here = start;

    for (i, command) in cmds.iter().cycle().enumerate() {
        let (left, right) = map.get(here).unwrap();
        here = match command {
            Command::Left => left,
            Command::Right => right,
        };
        if end(here) {
            return i + 1;
        }
    }

    0
}

pub fn solve_part1(input: &Input) -> usize {
    find_loop(input, "AAA", |x| x == "ZZZ")
}

pub fn solve_part2(input: &Input) -> usize {
    input
        .1
        .keys()
        .filter(|&x| x.ends_with('A'))
        .map(|start| find_loop(input, start, |x| x.ends_with('Z')))
        .reduce(|x, y| x.lcm(&y))
        .unwrap()
}
