pub fn input_generator(_: &str) {}

fn solve(races: &[(usize, usize)]) -> usize {
    races
        .iter()
        .map(|&(tmax, dmax)| (0..tmax).filter(|t| (t * (tmax - t)) > dmax).count())
        .product()
}

pub fn solve_part1(_: &()) -> usize {
    solve(&[(53, 275), (71, 1181), (78, 1215), (80, 1524)])
}

pub fn solve_part2(_: &()) -> usize {
    solve(&[(53717880, 275118112151524)])
}
