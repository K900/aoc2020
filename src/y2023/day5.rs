use nom::{
    bytes::complete::tag,
    character::complete::{alpha1, space1},
    multi::{separated_list0, separated_list1},
    sequence::tuple,
    IResult, Parser,
};
use rayon::iter::{IntoParallelIterator, ParallelIterator};

use crate::utils::parsers::number;

fn parse_seeds(stream: &str) -> IResult<&str, Vec<usize>> {
    tuple((tag("seeds: "), separated_list0(space1, number), tag("\n")))
        .map(|(_, ds, _)| ds)
        .parse(stream)
}

#[derive(Debug)]
pub struct Mapping {
    from: usize,
    to: usize,
    length: usize,
}

fn parse_mapping(stream: &str) -> IResult<&str, Mapping> {
    tuple((number, tag(" "), number, tag(" "), number))
        .map(|(to, _, from, _, length)| Mapping { from, to, length })
        .parse(stream)
}

#[derive(Debug)]
pub struct MappingSet {
    mappings: Vec<Mapping>,
}

impl MappingSet {
    fn apply(&self, i: usize) -> usize {
        for m in &self.mappings {
            if (m.from..m.from + m.length).contains(&i) {
                return i - m.from + m.to;
            }
        }
        i
    }
}

fn parse_mapping_set(stream: &str) -> IResult<&str, MappingSet> {
    separated_list1(tag("\n"), parse_mapping)
        .map(|mappings| MappingSet { mappings })
        .parse(stream)
}

fn parse_map(stream: &str) -> IResult<&str, MappingSet> {
    tuple((
        alpha1,
        tag("-to-"),
        alpha1,
        tag(" map:\n"),
        parse_mapping_set,
        tag("\n"),
    ))
    .map(|(_, _, _, _, m, _)| m)
    .parse(stream)
}

#[derive(Debug)]
pub struct Input {
    seeds: Vec<usize>,
    mappings: Vec<MappingSet>,
}

impl Input {
    fn apply_mappings(&self, i: usize) -> usize {
        self.mappings.iter().fold(i, |acc, m| m.apply(acc))
    }
}

fn parse_input(stream: &str) -> IResult<&str, Input> {
    tuple((
        parse_seeds,
        tag("\n"),
        separated_list1(tag("\n"), parse_map),
    ))
    .map(|(seeds, _, mappings)| Input { seeds, mappings })
    .parse(stream)
}

pub fn input_generator(input: &str) -> Input {
    parse_input(input).unwrap().1
}

pub fn solve_part1(input: &Input) -> usize {
    input
        .seeds
        .iter()
        .map(|&i| input.apply_mappings(i))
        .min()
        .unwrap()
}

pub fn solve_part2(input: &Input) -> usize {
    let mut s = vec![];

    for c in input.seeds.chunks_exact(2) {
        s.extend(c[0]..c[0] + c[1])
    }

    s.into_par_iter()
        .map(move |i| input.apply_mappings(i))
        .min()
        .unwrap()
}
