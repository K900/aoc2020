use nom::{
    branch::alt, bytes::complete::tag, character::complete::digit1, multi::separated_list0,
    sequence::tuple, IResult, Parser,
};

#[derive(Debug)]
pub enum Color {
    Red,
    Green,
    Blue,
}

fn parse_color(stream: &str) -> IResult<&str, Color> {
    alt((tag("red"), tag("green"), tag("blue")))
        .map(|c| match c {
            "red" => Color::Red,
            "green" => Color::Green,
            "blue" => Color::Blue,
            _ => unreachable!(),
        })
        .parse(stream)
}

#[derive(Debug)]
pub struct Hand(usize, Color);

fn parse_hand(stream: &str) -> IResult<&str, Hand> {
    tuple((digit1, tag(" "), parse_color))
        .map(|(x, _, c)| Hand(x.parse().expect("Failed to parse x"), c))
        .parse(stream)
}

#[derive(Debug)]
pub struct Set {
    red: usize,
    green: usize,
    blue: usize,
}

fn parse_set(stream: &str) -> IResult<&str, Set> {
    separated_list0(tag(", "), parse_hand)
        .map(|hands| {
            let mut result = Set {
                red: 0,
                green: 0,
                blue: 0,
            };

            for Hand(i, c) in hands {
                match c {
                    Color::Red => result.red = result.red.max(i),
                    Color::Green => result.green = result.green.max(i),
                    Color::Blue => result.blue = result.blue.max(i),
                }
            }

            result
        })
        .parse(stream)
}

fn parse_sets(stream: &str) -> IResult<&str, Vec<Set>> {
    separated_list0(tag("; "), parse_set).parse(stream)
}

#[derive(Debug)]
pub struct Game(usize, Vec<Set>);

fn parse_game(stream: &str) -> IResult<&str, Game> {
    tuple((tag("Game "), digit1, tag(": "), parse_sets))
        .map(|(_, x, _, s)| Game(x.parse().expect("Failed to parse x"), s))
        .parse(stream)
}

pub fn input_generator(input: &str) -> Vec<Game> {
    input
        .split('\n')
        .map(|s| parse_game(s).unwrap().1)
        .collect()
}

pub fn solve_part1(input: &[Game]) -> usize {
    input
        .iter()
        .filter(|Game(_, sets)| {
            sets.iter()
                .all(|set| set.red <= 12 && set.green <= 13 && set.blue <= 14)
        })
        .map(|Game(i, _)| i)
        .sum()
}

pub fn solve_part2(input: &[Game]) -> usize {
    input
        .iter()
        .map(|Game(_, sets)| {
            let mut best = Set {
                red: 0,
                green: 0,
                blue: 0,
            };
            for set in sets {
                best.red = best.red.max(set.red);
                best.green = best.green.max(set.green);
                best.blue = best.blue.max(set.blue);
            }
            best.red * best.green * best.blue
        })
        .sum()
}
