pub fn input_generator(input: &str) -> Vec<String> {
    input.split('\n').map(String::from).collect()
}

const DIGITS: [(&str, u32); 9] = [
    ("1", 1),
    ("2", 2),
    ("3", 3),
    ("4", 4),
    ("5", 5),
    ("6", 6),
    ("7", 7),
    ("8", 8),
    ("9", 9),
];

const WORDS: [(&str, u32); 9] = [
    ("one", 1),
    ("two", 2),
    ("three", 3),
    ("four", 4),
    ("five", 5),
    ("six", 6),
    ("seven", 7),
    ("eight", 8),
    ("nine", 9),
];

fn solve(input: &[String], patterns: &[(&str, u32)]) -> u32 {
    input
        .iter()
        .map(|s| {
            let first = patterns
                .iter()
                .min_by_key(|(p, _)| s.find(p).unwrap_or(usize::MAX))
                .unwrap()
                .1;
            let last = patterns
                .iter()
                .max_by_key(|(p, _)| s.rfind(p).map(|x| x + 1).unwrap_or(0))
                .unwrap()
                .1;
            first * 10 + last
        })
        .sum()
}

pub fn solve_part1(input: &[String]) -> u32 {
    solve(input, &DIGITS)
}

pub fn solve_part2(input: &[String]) -> u32 {
    solve(input, &[DIGITS, WORDS].concat())
}
