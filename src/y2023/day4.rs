use std::collections::HashSet;

use nom::{
    bytes::complete::tag, character::complete::digit1, character::complete::space1,
    multi::separated_list0, sequence::tuple, IResult, Parser,
};

fn parse_digit_set(stream: &str) -> IResult<&str, HashSet<usize>> {
    separated_list0(space1, digit1)
        .map(|c| c.into_iter().map(|x: &str| x.parse().unwrap()).collect())
        .parse(stream)
}

#[derive(Debug)]
pub struct Card(HashSet<usize>, HashSet<usize>);
impl Card {
    fn wins(&self) -> usize {
        self.0.intersection(&self.1).count()
    }
}

fn parse_card(stream: &str) -> IResult<&str, Card> {
    tuple((
        tag("Card"),
        space1,
        digit1,
        tag(":"),
        space1,
        parse_digit_set,
        space1,
        tag("|"),
        space1,
        parse_digit_set,
    ))
    .map(|(_, _, _, _, _, win, _, _, _, our)| Card(win, our))
    .parse(stream)
}

pub fn input_generator(input: &str) -> Vec<Card> {
    input
        .split('\n')
        .map(|s| parse_card(s).unwrap().1)
        .collect()
}

pub fn solve_part1(input: &[Card]) -> usize {
    input
        .iter()
        .map(|c| {
            let n = c.wins();
            if n == 0 {
                0
            } else {
                2usize.pow((n - 1) as u32)
            }
        })
        .sum()
}

pub fn solve_part2(input: &[Card]) -> usize {
    let mut counts = vec![1; input.len()];
    for (i, c) in input.iter().enumerate() {
        for j in (i + 1)..(i + 1 + c.wins()) {
            counts[j] += counts[i];
        }
    }

    counts.iter().sum()
}
