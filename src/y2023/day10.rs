use std::fmt::Debug;

use image::io::Reader as ImageReader;
use image::{GenericImageView, Rgb, RgbImage, Rgba};

use crate::utils::grid::Grid;

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Pipe {
    Empty,
    Start,
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight,
    TopBottom,
    LeftRight,
}

impl Pipe {
    fn neighbor_offsets(&self) -> [(isize, isize); 2] {
        match self {
            Pipe::Empty => unreachable!(),
            Pipe::Start | Pipe::TopLeft => [(0, -1), (-1, 0)],
            Pipe::TopRight => [(0, -1), (1, 0)],
            Pipe::BottomLeft => [(0, 1), (-1, 0)],
            Pipe::BottomRight => [(0, 1), (1, 0)],
            Pipe::TopBottom => [(0, 1), (0, -1)],
            Pipe::LeftRight => [(-1, 0), (1, 0)],
        }
    }
}

impl Debug for Pipe {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Empty => write!(f, " "),
            Self::Start => write!(f, "S"),
            Self::TopLeft => write!(f, "┘"),
            Self::TopRight => write!(f, "└"),
            Self::BottomLeft => write!(f, "┐"),
            Self::BottomRight => write!(f, "┌"),
            Self::TopBottom => write!(f, "│"),
            Self::LeftRight => write!(f, "─"),
        }
    }
}

impl Default for Pipe {
    fn default() -> Self {
        Self::Empty
    }
}

type G = Grid<Pipe, 140, 140>;

pub fn input_generator(input: &str) -> G {
    G::from_grid(input).map(|_, _, &c| match c {
        '|' => Pipe::TopBottom,
        '-' => Pipe::LeftRight,
        'L' => Pipe::TopRight,
        'J' => Pipe::TopLeft,
        '7' => Pipe::BottomLeft,
        'F' => Pipe::BottomRight,
        '.' => Pipe::Empty,
        'S' => Pipe::Start,
        _ => unreachable!(),
    })
}

fn find_path(g: &G) -> Vec<(usize, usize)> {
    let start = g.all_indices().find(|&i| g[i] == Pipe::Start).unwrap();
    let mut here = start;
    let mut path = vec![here];

    loop {
        for (dx, dy) in g[here].neighbor_offsets() {
            let newloc = (
                (here.0 as isize + dx) as usize,
                (here.1 as isize + dy) as usize,
            );
            if *path.last().unwrap() != newloc {
                path.push(here);
                here = newloc;
                break;
            }
        }
        if here == start {
            break;
        }
    }

    path
}

pub fn solve_part1(g: &G) -> usize {
    find_path(g).len() / 2
}

const WHITE: image::Rgb<u8> = Rgb([255, 255, 255]);

pub fn solve_part2(g: &G) -> usize {
    let mut img = RgbImage::new(140 * 3, 140 * 3);

    for (x, y) in find_path(g) {
        let pipe = g[(x, y)];
        let (cx, cy) = ((x * 3) as u32, (y * 3) as u32);

        img.put_pixel(cx, cy, WHITE);
        for (dx, dy) in pipe.neighbor_offsets() {
            let (nx, ny) = ((cx as isize + dx) as u32, (cy as isize + dy) as u32);
            img.put_pixel(nx, ny, WHITE);
        }
    }

    img.save("maze.png").unwrap();

    // image doesn't have flood fill. really? use krita or something lmao

    let img = ImageReader::open("maze_filled.png")
        .unwrap()
        .decode()
        .unwrap();

    g.all_indices()
        .filter(|(x, y)| {
            let (cx, cy) = ((x * 3) as u32, (y * 3) as u32);
            img.get_pixel(cx, cy) == Rgba([0, 255, 0, 255])
        })
        .count()
}
