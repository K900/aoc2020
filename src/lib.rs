#![feature(array_windows, box_patterns)]

pub mod utils;
pub mod y2020;
pub mod y2021;
pub mod y2022;
pub mod y2023;
